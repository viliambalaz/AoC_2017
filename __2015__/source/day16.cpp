#include <iostream>
#include <fstream>
#include <regex>
#include <map>
using namespace std;

map<string, int> account_data(void)
{
    map<string, int> account;
    account.emplace("children", 3);
    account.emplace("cats", 7);
    account.emplace("samoyeds", 2);
    account.emplace("pomeranians", 3);
    account.emplace("akitas", 0);
    account.emplace("vizslas", 0);
    account.emplace("goldfish", 5);
    account.emplace("trees", 3);
    account.emplace("cars", 2);
    account.emplace("perfumes", 1);
    return account;
}

int part_1(ifstream& file, map<string, int> account)
{
    regex e("(\\w+): (\\d+)");
    smatch m;
    string line;
    int i = 0;
    while(getline(file, line))
    {
        bool match = true;
        i++;
        string s = line;
        while(regex_search(s, m, e))
        {
            if(account.at(m[1]) != stoi(m[2])){
                match = false;
                break;
            }
            s = m.suffix().str();
        }
        if(match){
            return i;
        }
    }
    return -1;
}

int part_2(ifstream&file, map<string, int> account)
{
    regex e("(\\w+): (\\d+)");
    smatch m;
    string line;
    int i = 0;
    while(getline(file, line))
    {
        bool match = true;
        i++;
        string s = line;
        while(regex_search(s, m, e))
        {
            if(m[1] == "cats" || m[1] == "trees"){
                 if(account.at(m[1]) > stoi(m[2])){
                    match = false;
                    break;
                 }
            }
            else if((m[1] == "pomeranians" || m[1] == "goldfish")){
                if(account.at(m[1]) < stoi(m[2])){
                match = false;
                break;
                }
            }
            else{
                if(account.at(m[1]) != stoi(m[2])){
                    match = false;
                    break;
                }
            }
            s = m.suffix().str();
        }
        if(match){
            return i;
        }
    }
    return -1;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }

    map<string, int> account = account_data();
    cout << "part one: " << part_1(file, account) << endl;
    file.clear();
    file.seekg(0);
    cout << "part two: " << part_2(file, account) << endl;
    file.close();
    return 0;
}

