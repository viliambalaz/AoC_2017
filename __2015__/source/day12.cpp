#include <iostream>
#include <fstream>
#include <regex>
#include <string>
using namespace std;

int part_1(const string dt)
{
    int sum = 0;
    string data = dt;
    regex e("(-?\\d+)");
    smatch m;
    while(regex_search(data, m, e))
    {
        sum += stoi(m[1]);
        data = m.suffix().str();
    }
    return sum;
}

pair<int, int> bracket_ix(const string dt, int red)
{
    pair<int, int> ixs;
    int n = 1;
    int m = 1;
    int i = red;
    //search begin of bracket
    while(n && m)
    {
        switch(dt[i])
        {
            case '{':
            {
                n--;
                break;
            }
            case '}':
            {
                n++;
                break;
            }
            case '[':
            {
                m--;
                break;
            }
            case ']':
            {
                m++;
                break;
            }
        }
        i--;
    }
    if(!m){
        // red is inside []
        return {red, red+3};
    }
    ixs.first = i + 1;
    //search end of the bracket
    i = red;
    n = 1; // {}
    m = 1; // []
    while(n)
    {
        switch(dt[i])
        {
            case '}':
            {
                n--;
                break;
            }
            case '{':
            {
                n++;
                break;
            }
        }
        i++;
    }
    ixs.second = i;
    return ixs;
}

int part_2(const string dt)
{
    string data = dt;
    size_t red = data.find("red");
    //cout << data << endl;
    //cout << red << endl;
    while(red != string::npos)
    {
        pair<int, int> brackets = bracket_ix(data, red);
        int x = brackets.first;
        int y = brackets.second;
        data.erase(data.begin()+x, data.begin()+y);
        red = data.find("red");
    }
    return part_1(data);
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string data;
    file >> data;
    file.close();
    cout << "part one: " << part_1(data) << endl;
    cout << "part two: " << part_2(data) << endl;
    return 0;
}

