#include <iostream>
#include <fstream>
#include <regex>
using namespace std;

int generate_code(int h, int w)
{
    long code = 20151125;
    int base = 252533;
    int mod = 33554393;
    int loop = (h + w - 2) * (h + w - 1) / 2 + w - 1;
    for(int i = 0; i < loop; i++)
        code = (code * base) % mod;
    return code;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string line;
    smatch m;
    regex e("\\D*(\\d+)\\D*(\\d+)\\D*");
    file >> line;
    getline(file, line);
    file.close();
    regex_match(line, m, e);
    int h = stoi(m[1]);
    int w = stoi(m[2]);

    cout << "part one: " << generate_code(h, w) << endl;
    return 0;
}

