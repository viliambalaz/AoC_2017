#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <map>
#include <vector>
#include <set>
#include <chrono>
#include <random>
#include <algorithm>
using namespace std;

vector<string> split_molecule(string molecule)
{
    vector <string> atoms;
    regex e("[A-Z][a-z]?"); // match atoms in periodic table of elements
    smatch m;
    while(regex_search(molecule, m, e))
    {
        atoms.push_back(m[0]);
        molecule = m.suffix().str();
    }
    return atoms;
}

string join_molecule(vector<string> atoms)
{
    string molecule;
    for(string a: atoms){
        molecule += a;
    }
    return molecule;
}

string replace_atom(vector<string> atoms, int ix, string a)
{
    atoms.at(ix) = a;
    return join_molecule(atoms);
}

int count_substr(string s, string target)
{
   int occurrences = 0;
   string::size_type pos = 0;
   while ((pos = s.find(target, pos )) != string::npos) {
          ++ occurrences;
          pos += target.length();
   }
    return occurrences;
}

int part_1(string molecule, map<string, vector<string> > replacements)
{
    vector<string> atoms = split_molecule(molecule);
    set<string> distinct;
    for(int i = 0; i < (int)atoms.size(); i++)
    {
        if(replacements.count(atoms[i]) == 0){
            continue;
        }
        for(string a: replacements.at(atoms[i]))
        {
            distinct.insert(replace_atom(atoms, i, a));
        }
    }
    return distinct.size();
}

int part_2(string molecule, vector<pair<string, string> > rreplacements)
{

    auto cmp = [ ]( const pair<string, string>& a, const pair<string, string>& b )
    {
        return (a.first.length() - a.second.length()) > (b.first.length() - b.second.length());
    };
    sort(rreplacements.begin(), rreplacements.end(), cmp);

    int steps = 0;
    string tmp = molecule;
    while(tmp != "e")
    {
        string tmp2 = tmp;
        for(auto r: rreplacements)
        {
            regex e("(.*)(" + r.first + ")(.*)");
            smatch m;
            if(regex_match(tmp, m, e)){
                tmp = regex_replace(tmp, e, "$1"+r.second+"$3");
                steps++;
            }
        }
        if(tmp == tmp2){
            /*
            // cannot pack into 'e'
            // shuffle replacements and try again
            tmp = molecule;
            unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
            shuffle (rreplacements.begin(), rreplacements.end(), std::default_random_engine(seed));
            steps = 0;
            //
            */
            return -1;
        }
    }
    return steps;
}

int part_2(string molecule)
{
    /* mathematical calculating
        -> each atom is expand into 2 another atoms;
        -> atoms Rn and Ar are free
        -> atom Y bind another atom
    */
    int Rn = count_substr(molecule, "Rn");
    int Ar = count_substr(molecule, "Ar");
    int Y = count_substr(molecule, "Y");
    int n = split_molecule(molecule).size();
    return n - (Rn + Ar) - 2*Y - 1;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    map<string, vector<string> > replacements;
    vector<pair<string, string> > rreplacements;
    string line, molecule;
    smatch m;
    regex e("(\\w+) => (\\w+)");
    while(getline(file, line))
    {
        if(regex_match(line, m, e)){
            replacements[m[1]].push_back(m[2]);
            rreplacements.push_back({m[2],m[1]});
        }
        else{
            molecule = line;
        }
    }
    file.close();
    cout << "part one: " << part_1(molecule, replacements) << endl;
    //cout << "part two: " << part_2(molecule, rreplacements) << endl;
    cout << "part two: " << part_2(molecule) << endl;
    return 0;
}

