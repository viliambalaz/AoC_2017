#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>
#include <set>
#include <vector>
#include <map>
using namespace std;

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    map<pair<string, string>, int> graph;
    set<string> places_tmp;
    string _eq_, source, destination, to;
    int distance;
    while(file >> source >> to >> destination >> _eq_ >> distance)
    {
        places_tmp.insert(source);
        places_tmp.insert(destination);
        graph[{source, destination}] = distance;
        graph[{destination, source}] = distance;
    }
    file.close();
    vector<string> places(places_tmp.begin(), places_tmp.end());
    int shortest = 0xffffff, longest = 0, route;
    vector<string>::iterator it;

    do{
        route = 0;
        for(it = places.begin(); it != prev(places.end()); it++){
            route += graph[{*it, *next(it)}];
        }
        shortest = route < shortest ? route : shortest;
        longest = route > longest ? route : longest;
    }while(next_permutation(places.begin(), places.end()) );

    cout << "part one: " << shortest << endl;
    cout << "part two: " << longest << endl;

    return 0;
}
