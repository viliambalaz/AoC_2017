#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <tuple>
using namespace std;

void equipments(vector<tuple<int, int, int> >& weapons, vector<tuple<int, int, int> >& armor, vector<tuple<int, int, int> >& rings)
{
    weapons.push_back(make_tuple(8, 4, 0));
    weapons.push_back(make_tuple(10, 5, 0));
    weapons.push_back(make_tuple(25, 6, 0));
    weapons.push_back(make_tuple(40, 7, 0));
    weapons.push_back(make_tuple(74, 8, 0));

    armor.push_back(make_tuple(13, 0, 1));
    armor.push_back(make_tuple(31, 0 ,2));
    armor.push_back(make_tuple(53, 0, 3));
    armor.push_back(make_tuple(75, 0, 4));
    armor.push_back(make_tuple(102, 0, 5));
    armor.push_back(make_tuple(0, 0, 0)); //any

    rings.push_back(make_tuple(25, 1, 0));
    rings.push_back(make_tuple(50, 2, 0));
    rings.push_back(make_tuple(100, 3, 0));
    rings.push_back(make_tuple(20, 0, 1));
    rings.push_back(make_tuple(40, 0 ,2));
    rings.push_back(make_tuple(80, 0, 3));
    rings.push_back(make_tuple(0, 0, 0)); // any ring
    rings.push_back(make_tuple(0, 0, 0));
    return;
}

bool roundup(const tuple<int, int, int>& player, const tuple<int, int, int>& boss)
{
    int p_hp, p_dmg, p_arm;
    int b_hp, b_dmg, b_arm;
    tie(p_hp, p_dmg, p_arm) = player;
    tie(b_hp, b_dmg, b_arm) = boss;
    int p_rounds = ceil(((double)p_hp / max(b_dmg - p_arm, 1)));
    int b_rounds = ceil(((double)b_hp / max(p_dmg - b_arm, 1)));
    return p_rounds >= b_rounds;
}

void add(tuple<int, int, int> item, int& cost, int& dmg, int& arm)
{
    cost += get<0>(item);
    dmg += get<1>(item);
    arm += get<2>(item);
    return;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    file.close();
    //   <hp,  dmg, arm>
    tuple<int, int, int> player(100, 0, 0);
    tuple<int, int, int> boss(104, 8, 1);

    //          <cst, dmg, arm>
    vector<tuple<int, int, int> > weapons;
    vector<tuple<int, int, int> > armor;
    vector<tuple<int, int, int> > rings;
    equipments(weapons, armor, rings);

    vector<tuple<int, int, int> >::iterator left_hand;
    vector<tuple<int, int, int> >::iterator right_hand;
    int part_1 = 10000, part_2 = 0;

    for(auto w: weapons)
    {
        for(auto a: armor)
        {
            for(left_hand = rings.begin(); left_hand != prev(rings.end()); left_hand++){
                for(right_hand = next(left_hand); right_hand != rings.end(); right_hand++){
                    int cost = 0, dmg = 0, arm = 0;
                    add(w, cost, dmg, arm);
                    add(a, cost, dmg, arm);
                    add(*left_hand, cost, dmg, arm);
                    add(*right_hand, cost, dmg, arm);
                    tuple<int, int, int> tmp(get<0>(player), get<1>(player) + dmg, get<2>(player) + arm);

                    if(roundup(tmp, boss) && (cost < part_1)){
                        part_1 = cost;
                    }
                    if(!roundup(tmp, boss) && (cost > part_2)){
                        part_2 = cost;
                    }
                }
            }
        }
    }

    cout << "part one: " << part_1 << endl;
    cout << "part two: " << part_2 << endl;
    return 0;
}
