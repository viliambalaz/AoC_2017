#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <cmath>
using namespace std;

void increase_string(string& s, int ix)
{
    if(ix == -1){
        s.insert(0, 1, 'a');
        return;
    }
    if (s[ix] != 'z'){
        s[ix]++;
    }
    else{
        s[ix] = 'a';
        increase_string(s, ix-1);
    }
    return;
}

string get_e1()
{
    string e1 = ".*(";
    string alphabet = "abcdefghijklmnopqrstuvwxyz";
    for(unsigned int i=0; i< alphabet.length()-2; i++){
        e1 +=  alphabet.substr(i, 3) + '|';
    }
    e1.erase(e1.end()-1);
    e1 += ").*";
    return e1;
}

string solve_part(string puzzle)
{
    regex e1(get_e1());
    regex e2(R"(.*[iol].*)");
    regex e3(R"(.*(.)\1.*(.)\2.*)");
    smatch m;
    while(1)
    {
        increase_string(puzzle, puzzle.length()-1);
        if (regex_match(puzzle, m, e1) &&
            !regex_match(puzzle, m, e2) &&
            regex_match(puzzle, m, e3)){
            return puzzle;
        }
    }
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string puzzle, p1, p2;
    file >> puzzle;
    file.close();
    p1 = solve_part(puzzle);
    p2 = solve_part(p1);
    cout << "part 1: " << p1 << endl;
    cout << "part 2: " << p2 << endl;
    return 0;
}
