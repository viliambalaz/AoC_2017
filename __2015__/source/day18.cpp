#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#define row 100
#define col 100
using namespace std;

void print_grid(vector<vector<int> > led_grid)
{
    for(vector<int> vec : led_grid){
        for(int led : vec){
            cout << led << " ";
        }
        cout << endl;
    }
}

int on_neigh(vector<vector<int> >& led_grid, int x, int y)
{
    int on = 0;
    vector<pair<int, int> > direction;
    for(int i = -1; i <= 1; i++){
        for(int j = -1; j <= 1; j++){
            if( i == 0 && j == 0){continue;}
            direction.push_back({i,j});
        }
    }
    auto valid_co = [](int x, int y){ return (x >= 0) && (x < row) && (y >= 0) && (y < col); };
    for(pair<int, int> d : direction)
    {
        if(valid_co(x+d.first, y+d.second) ){
            on += led_grid[x+d.first][y+d.second];
        }
    }
    return on;
}

int on_grid(vector<vector<int> >& led_grid)
{
    int on = 0;
    for(vector<int> vec : led_grid){
        for(int led : vec){
            on += led;
        }
    }
    return on;
}

void turn(vector<vector<int> >& led_grid, bool part_2=false)
{
    vector<vector<int> > tmp = led_grid;
    for(int i=0; i < row; i++){
        for(int j=0; j < col; j++){
            int on = on_neigh(led_grid, i, j);
            if(part_2 && ( ((i == 0) || (i == row-1)) && ((j == 0) || (j == col -1)) )){
                continue;
            }
            if(led_grid[i][j] == 1){
                if((on != 2) && (on != 3)){
                    tmp[i][j] = 0;
                }
            }
            else{ //(led_grid[i][j] == 0)
                if(on == 3){
                    tmp[i][j] = 1;
                }
            }
        }
    }
    led_grid = tmp;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    vector<vector<int> > led_grid(row, vector<int>());
    int i = 0, j = 0;
    char c;
    while(file >> c){
        int state = (c == '#') ? 1 : 0;
        led_grid[j].push_back(state);
        if((++i % col) == 0){
            j++;
        }
    }
    file.close();

    vector<vector<int> > led_grid2 = led_grid;
    led_grid2[0][0] = 1;
    led_grid2[0][col -1] = 1;
    led_grid2[row -1][0] = 1;
    led_grid2[row -1][col -1] = 1;

    for(int n = 0; n < 100; n++)
    {
        turn(led_grid);
        turn(led_grid2, true);
    }

    cout << "part one: " << on_grid(led_grid) << endl;
    cout << "part two: " << on_grid(led_grid2) << endl;
    return 0;
}
