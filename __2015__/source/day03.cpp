#include <iostream>
#include <fstream>
#include <set>
using namespace std;

void santa_move(ifstream& file, set<pair<int, int> >& visited, int& x, int& y, char c)
{
    switch (c)
    {
        case '^':{
            y += 1;
            break;
        }
        case 'v':{
            y -= 1;
            break;
        }
        case '>':{
            x += 1;
            break;
        }
        case '<':{
            x -= 1;
            break;
        }
        default:{
            cerr << "Wrong character read." << endl;
        }
    }
    visited.insert({x,y});
}

int solve_part(ifstream& file, bool part2=false)
{
    char c;
    bool robot = false;
    set <pair<int, int> >visited;
    visited.insert({0,0});
    int x_s, y_s, x_r, y_r;
    x_s = y_s = x_r = y_r = 0;
    while(file >> c)
    {
        if(part2 && robot){
            santa_move(file, visited, x_r, y_r, c);
            robot = false;
        }
        else{
            santa_move(file, visited, x_s, y_s, c);
            robot = true;
        }
    }
    return visited.size();
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    cout << "part 1: " << solve_part(file) << endl;
    file.clear();
    file.seekg(0);
    cout << "part 2: " << solve_part(file, true) << endl;
    file.close();
    return 0;
}
