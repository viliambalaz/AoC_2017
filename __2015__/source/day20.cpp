#include <iostream>
#include <fstream>
#include <climits>
using namespace std;

int solve_part(int target, int m, int limit)
{
    int n = 2000000; //guess max no. of house
    int* houses = new int[n];
    for(int i = 1; i < n; i++){
        for(int j = 1; j < min(n / i, limit); j++){
            houses[i*j -1] += i * m;
        }
        if(houses[i-1] >= target){
            delete[] houses;
            return i;
        }
    }
    delete[] houses;
    return -1;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    int target;
    file >> target;
    file.close();

    cout << "part one: " << solve_part(target, 10, INT_MAX) << endl;
    cout << "part two: " << solve_part(target, 11, 50) << endl;
    return 0;
}

