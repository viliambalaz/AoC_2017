#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#define row 1000
#define column 1000
using namespace std;

int get_sum(int field[row][column])
{
    int sum = 0;
    for(int i = 0; i < row; i++)
        for(int j = 0; j < column; j++)
            sum += field[i][j];
    return sum;
}

int part_1(ifstream& file)
{
    int field[row][column];
    for(int i = 0; i < row; i++)
        for(int j = 0; j < column; j++)
            field[i][j] = 0;
    string line;
    regex e("([a-zA-Z ]*)(\\d*),(\\d*) through (\\d*),(\\d*)");
    while(getline(file, line))
    {
        string op;
        int x1, y1, x2, y2;
        smatch m;
        string tmp = line;
        regex_match(tmp, m, e);
        op = m[1]; x1 = stoi(m[2]); y1 = stoi(m[3]); x2 = stoi(m[4]); y2 = stoi(m[5]);
        if(op == "turn on "){
            for(int i = x1; i <= x2; i++)
                for(int j = y1; j <= y2; j++)
                    field[i][j] = 1;
        }
        else if(op == "turn off "){
            for(int i = x1; i <= x2; i++)
                for(int j = y1; j <= y2; j++)
                    field[i][j] = 0;
        }
        else if(op == "toggle "){
            for(int i = x1; i <= x2; i++)
                for(int j = y1; j <= y2; j++)
                    field[i][j] ^= 1;
        }
        else{
            cout << "Wrong instruction read." << endl;
            return -3;
        }
    }
    return get_sum(field);
}

int part_2(ifstream& file)
{
    int field[row][column];
    for(int i = 0; i < row; i++)
        for(int j = 0; j < column; j++)
            field[i][j] = 0;
    string line;
    regex e("([a-zA-Z ]*)(\\d*),(\\d*) through (\\d*),(\\d*)");
    while(getline(file, line))
    {
        string op;
        int x1, y1, x2, y2;
        smatch m;
        string tmp = line;
        regex_match(tmp, m, e);
        op = m[1]; x1 = stoi(m[2]); y1 = stoi(m[3]); x2 = stoi(m[4]); y2 = stoi(m[5]);
        if(op == "turn on "){
            for(int i = x1; i <= x2; i++)
                for(int j = y1; j <= y2; j++)
                    field[i][j] += 1;
        }
        else if(op == "turn off "){
            for(int i = x1; i <= x2; i++)
                for(int j = y1; j <= y2; j++)
                    if(field[i][j]) field[i][j] -= 1;
        }
        else if(op == "toggle "){
            for(int i = x1; i <= x2; i++)
                for(int j = y1; j <= y2; j++)
                    field[i][j] += 2;
        }
        else{
            cout << "Wrong instruction read." << endl;
            return -3;
        }
    }
    return get_sum(field);
}
int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    cout << "part one: " << part_1(file) << endl;
    file.clear();
    file.seekg(0);
    cout << "part two: " << part_2(file) << endl;
    file.close();
    return 0;
}

