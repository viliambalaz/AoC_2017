#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/md5.h>
using namespace std;
// linking with -lcrypt
// http://www.askyb.com/cpp/openssl-md5-hashing-example-in-cpp/

int md5_hash(char input[], int n)
{
    char zeros[n+1];
    sprintf(zeros, "%0*d", n, 0);
    for(int i=0; ; i++)
    {
        char puzzle[100];
        sprintf(puzzle, "%s%d", input, i);

        unsigned char digest[MD5_DIGEST_LENGTH];
        MD5((unsigned char*)&puzzle, strlen(puzzle), (unsigned char*)&digest);
        char mdString[33];
        for(int j = 0; j < (n+1)/2; j++){
            sprintf(&mdString[j*2], "%02x", (unsigned int)digest[j]);
        }
        if(!strncmp(zeros, mdString, n))
            return i;
    }
}

int main(int argc, char** argv)
{
    FILE *file;
    file = fopen(argv[1], "r");
    if(file == NULL){
        fprintf(stderr, "Can't open the file.\n");
        return -1;
    }
    char input[15];
    fscanf(file, "%s", input);
    fclose(file);
    printf("part one: %d\n", md5_hash(input, 5));
    printf("part two: %d\n", md5_hash(input,6));
    return 0;
}
