#include <iostream>
#include <fstream>
using namespace std;

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    int floor = 0;
    int i = 0, j = 0;
    char c;
    while(file >> c)
    {
        if(floor < 0 && !j){
            j = i;
        }
        i++;
        switch (c)
        {
            case '(':{
                floor++;
                break;
            }
            case ')':{
                floor--;
                break;
            }
            default:{
                cerr << "Wrong character read" << endl;
                return -2;
            }
        }
    }
    file.close();
    cout << "part 1: " << floor << endl;
    cout << "part 2: " << j << endl;
    return 0;
}
