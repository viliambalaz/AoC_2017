#include <iostream>
#include <fstream>
#include <regex>
using namespace std;
/// solve for specific input: 4 ingredience with 5 attributes

int solve_part(int data[4][5], bool part_2=false)
{
    int max_score = 0;
    for(int a = 0; a < 100; a++){
        for(int b = 0; b < 100 - a; b++){
            for(int c = 0; c < 100 - a - b; c++)
            {
                int d = 100 - a - b - c;
                int v, w, x, y, z;
                v = (a * data[0][0]) + (b * data[1][0]) + (c * data[2][0]) + (d * data[3][0]);
                w = (a * data[0][1]) + (b * data[1][1]) + (c * data[2][1]) + (d * data[3][1]);
                x = (a * data[0][2]) + (b * data[1][2]) + (c * data[2][2]) + (d * data[3][2]);
                y = (a * data[0][3]) + (b * data[1][3]) + (c * data[2][3]) + (d * data[3][3]);
                z = (a * data[0][4]) + (b * data[1][4]) + (c * data[2][4]) + (d * data[3][4]);
                if( (v <= 0) || (w <= 0) || (x <= 0) || (y <= 0) || (z <= 0)){
                    continue;
                }
                if(part_2 && z != 500){
                    continue;
                }
                int score = v * w * x * y;
                max_score = max(max_score, score);
            }
        }
    }
    return max_score;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    regex e("\\w+: capacity (-?\\d+), durability (-?\\d+), flavor (-?\\d+), texture (-?\\d+), calories (-?\\d+)");
    smatch m;
    string line;
    int data[4][5];
    for(int i=0; i<4; i++)
    {
        getline(file, line);
        regex_match(line, m, e);
        for(int j = 1; j < 5+1; j++){
            data[i][j-1] = stoi(m[j]);
        }
    }
    file.close();

    cout << "part one: " << solve_part(data) << endl;
    cout << "part two: " << solve_part(data, true) << endl;
    return 0;
}

