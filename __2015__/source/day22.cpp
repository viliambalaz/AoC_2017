///In progress
#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <list>
#include <map>
using namespace std;

class Spell
{
public:
    string name;
    int mana_cost;
    int damage;
    int heal;
    int armor;
    int mana_recharge;
    int duration;

    Spell(string name, int mana_cost, int damage, int heal, int armor, int mana_recharge, int duration)
    {
        this->name = name;
        this->mana_cost = mana_cost;
        this->damage = damage;
        this->heal = heal;
        this->armor = armor;
        this->mana_recharge = mana_recharge;
        this->duration = duration;
    }

    int get_mana_cost()
    {
        return this->mana_cost;
    }
};

class Hero
{
public:
    string name;
    int damage;
    int armor;
    int hitpoints;
    int manapoints;
    map<string, Spell*> magic_book;
    map<Spell*, int>effects;  //Spell and duration of spell

public:
    //temporary stast using effects
    int dmg;
    int arm;

    Hero(string name, int dmg, int arm, int hp, int mana, map<string, Spell*> book)
    {
        this->name = name;
        this->damage = dmg;
        this->armor = arm;
        this->hitpoints = hp;
        this->manapoints = mana;
        this->magic_book = book;
    }

    void apply_effects()
    {
        this->dmg = 0;
        this->arm = 0;
        map<Spell*, int>::iterator it;
        for(it=effects.begin(); it != effects.end(); it++)
        {
            Spell* cast = (*it).first;
            this->arm += cast->armor;
            this->manapoints -= cast->mana_cost;
            this->manapoints += cast->mana_recharge;
            this->hitpoints -= cast->damage;
            this->hitpoints += cast->heal;
            (*it).second -= 1; //decrease duration
            if( (*it).second == 0)
            {
                this->effects.erase(it);
                it--;
            }
        }
    }

    void castSpell(Hero* target, string spellName)
    {
        if(this->magic_book.count(spellName) == 0){
            cout << "You don't know this spell." << endl;
            return;
        }
        if(this->magic_book[spellName]->mana_cost > this->manapoints){
            cout << "Out of mana" << endl;
            return;
        }

        target->effects[this->magic_book[spellName]] = this->magic_book[spellName]->duration;
        return;
    }

    void attack(Hero *opponent)
    {
        opponent->hitpoints -= max(this->damage + this->dmg - opponent->armor - opponent->arm, 1);
    }

    bool is_alive()
    {
        return (this->hitpoints >= 0) ? true : false;
    }

    int turn(Hero *opponent, Hero *target, string spellName)
    {
        if(this->is_alive() == false){
            cout << " you died: " << this->name << endl;
            return 1;
        }
        this->apply_effects();
        this->castSpell(target, spellName);
        this->attack(opponent);
        return 0;
    }
};


int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string line;
    smatch m1, m2;
    regex e("\\D*(\\d+)\\D*");
    getline(file, line);
    regex_match(line, m1, e);
    getline(file, line);
    regex_match(line, m2, e);

    int boss_hp = stoi(m1[1]);
    int boss_dmg = stoi(m2[1]);
    file.close();

    map<string, Spell*> magic_book;
    magic_book["Magic Missile"] = new Spell("Magic Missile", 53, 4, 0, 0, 0, 1);
    magic_book["Drain"] = new Spell("Drain", 73, 2, 2, 0, 0, 1);
    magic_book["Shield"] = new Spell("Shield", 113, 0, 0, 7, 0, 6);
    magic_book["Poison"] = new Spell("Poison", 173, 3, 0, 0, 0, 6);
    magic_book["Recharge"] = new Spell("Recharge", 229, 0, 0, 0, 101, 5);
    magic_book["ANY_SPELL"] = new Spell("ANY_SPELL", 0, 0, 0, 0, 0, 0);

    Hero* player = new Hero("player", 0, 0, 50, 250, magic_book);
    Hero* boss = new Hero("boss", boss_dmg, 0, boss_hp, 0, magic_book);

    delete player;
    delete boss;
    return 0;
}
