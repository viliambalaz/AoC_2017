#include <iostream>
#include <fstream>
#include <regex>
#include <string>
using namespace std;

/*shell:
    cat input | grep "[aeiou].*[aeiou].*[aeiou]" | grep "\(.\)\1" | egrep -v "(ab|cd|pq|xy)" | wc -l
    cat input | grep "\(..\).*\1" | grep "\(.\).\1" | wc -l
*/

int part_1(ifstream& file)
{
    int valid = 0;
    string line;
    while(getline(file, line))
    {
        smatch m;
        regex e1("(\\w)\\1");
        regex e2("([aeiou])");
        regex e3("ab|cd|pq|xy");
        string tmp = line;
        int vowels = 0;
        bool t2 = false;
        while(regex_search(tmp,m,e2)) {
            tmp = m.suffix().str();
            if(++vowels == 3) t2 = true;
        }
        if(regex_search(line,m,e1) && t2 && !regex_search(line,m,e3)) valid++;
    }
    return valid;
}

int part_2(ifstream& file)
{
    int valid = 0;
    string line;
    smatch m;
    regex e1("(..).*\\1");
    regex e2("(.).\\1");
    while(getline(file, line)){
        if(regex_search(line, m, e1) && regex_search(line, m, e2)) valid++;
    }
    return valid;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    cout << "part one: " << part_1(file) << endl;
    file.clear();
    file.seekg(0);
    cout << "part two: " << part_2(file) << endl;
    file.close();
    return 0;
}

