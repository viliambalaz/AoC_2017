#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <map>
using namespace std;

class Raindeer
{
private:
    int speed;
    int run;
    int rest;
public:
    int distance = 0;
    int points = 0;

    Raindeer(int _speed, int _run, int _rest)
    {
        this->speed = _speed;
        this->run = _run;
        this->rest = _rest;
    }

    void turn(int sec)
    {
        int k = sec % (this->run + this->rest);
        if(k < this->run){
            this->distance += speed;
        }
    }

    int odo(int sec)
    {
        // return distance after `sec` seconds
        int dist = 0;
        int k = sec / (this->run + this->rest);
        int m = sec % (this->run + this->rest);
        m = (m > this->run) ? this->run : m;
        dist += k * this->run*this->speed;
        dist += m * this->speed;
        return dist;
    }
};

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    regex e("(\\w+) can fly (\\d+) km/s for (\\d+) seconds, but then must rest for (\\d+) seconds.");
    smatch m;
    string line;
    map<string, Raindeer> deers;
    while(getline(file, line))
    {
        regex_match(line, m, e);
        deers.emplace(m[1], Raindeer(stoi(m[2]), stoi(m[3]), stoi(m[4])));
    }
    file.close();

    int leading_distance = 0;
    for(int s = 0; s < 2503; s++)
    {
        vector<string> leaders;
        for(auto &d : deers)
        {
            d.second.turn(s);
            if(d.second.distance > leading_distance){
                leaders.clear();
                leaders.push_back(d.first);
                leading_distance = d.second.distance;
            }
            else if(d.second.distance == leading_distance){
                leaders.push_back(d.first);
            }
        }
        for(string name: leaders){
            deers.at(name).points += 1;
        }
    }
    int max_points = 0;
    int max_distance = 0;
    for(auto &d : deers){
        max_distance = max(max_distance, d.second.distance);
        max_points = max(max_points, d.second.points);
    }
    cout << "part one: " << max_distance << endl;
    cout << "part two: " << max_points << endl;
    return 0;
}

