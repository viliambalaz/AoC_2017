#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int part_1(ifstream& file)
{
    int total = 0;
    char c;
    while(file.get(c))
    {
        switch (c)
        {
        case '\n':{
            total += 2;
            break;
        }
        case '"':{
            total += 0;
            break;
        }
        case '\\':{
            file >> c;
            switch(c)
            {
                case('x'):{
                    total += 3;
                    break;
                }
                default:{
                    total += 1;
                    break;
                }
            }
            break;
        }
        }
    }
    return total;
}

int part_2(ifstream& file)
{
    int total = 0;
    char c;
    while(file.get(c))
    {
        switch (c)
        {
        case '\n':{
            total += 2;
            break;
        }
        case '"':{
            total += 1;
            break;
        }
        case '\\':{
           total += 1;
           break;
        }
        }
    }
    return total;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }

    cout << "part one: " << part_1(file) << endl;
    file.clear();
    file.seekg(0);
    cout << "part two: " << part_2(file) << endl;
    file.close();
    return 0;
}

