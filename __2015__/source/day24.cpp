#include <iostream>
#include <fstream>
#include <algorithm>
#include <numeric>
#include <climits>
using namespace std;
/**
Solution solving k-partrition problem.
Do not guarantee always good solution, not the minimum qe, than numbers need to be shuffled.
*/

// Function to check if all subsets are filled or not
bool checkSum(int sumLeft[], int k)
{
	int r = true;
	for (int i = 0; i < k; i++)
	{
		if (sumLeft[i] != 0)
			r = false;
	}

	return r;
}

// Helper function for solving k partition problem
// It return true if there exists k subsets with given sum
bool subsetSum(int S[], int n, int sumLeft[], int A[], int k)
{
	// return true if subset is found
	if (checkSum(sumLeft, k))
		return true;

	// base case: no items left
	if (n < 0)
		return false;

	bool res = false;

	// consider current item S[n] and explore all possibilities
	// using backtracking
	for (int i = 0; i < k; i++)
	{
		if (!res && (sumLeft[i] - S[n]) >= 0)
		{
			// mark current element subset
			A[n] = i + 1;

			// add current item to i'th subset
			sumLeft[i] = sumLeft[i] - S[n];

			// recurse for remaining items
			res = subsetSum(S, n - 1, sumLeft, A, k);

			// backtrack - remove current item from i'th subset
			sumLeft[i] = sumLeft[i] + S[n];
		}
	}
	// return true if we get solution
	return res;
}

// Function for solving k-partition problem. split set into subsets if
// set S[0..n-1] can be divided into k subsets with equal sum
long partition(int S[], int n, int k)
{
	// base case
	if (n < k)
	{
		cout << "k-Partition of set S is not Possible" << endl;
		return -1;
	}

	// get sum of all elements in the set
	int sum = accumulate(S, S + n, 0);
    if(sum % k != 0)
    {
        cout << "k-Partition of set S is not Possible" << endl;
		return -1;
    }
 	int A[n], sumLeft[k];

 	// create an array of size k for each subset and initialize it
 	// by their expected sum i.e. sum/k
	for (int i = 0; i < k; i++)
		sumLeft[i] = sum / k;

	// return true if sum is divisible by k and the set S can
	// be divided into k subsets with equal sum
	bool res = subsetSum(S, n - 1, sumLeft, A, k);
	if (!res)
	{
		cout << "k-Partition of set S is not Possible";
		return -1;
	}

	// all patrition
	int p_min = n; // minimum elements in subset
	long qe_min = LONG_MAX;
	for (int i = 0; i < k; i++)
	{
	    int p = 0; // get lowest qe from minimum items
	   	for (int j = 0; j < n; j++){
	 		if (A[j] == i + 1){
                p++;
	 		}
	   	}
	 	if(p < p_min)
        {
            p_min = p;
            long qe = 1;
            for (int j = 0; j < n; j++)
            {
                if (A[j] == i + 1){
                    qe *= S[j];
                }
            }
            qe_min = min(qe_min, qe);
        }
	}
	return qe_min;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    int x, n = 0;
    int arr[100];
    while(file >> x){
        arr[n++] = x;
    }
    file.close();
    sort(arr, arr + n);
    cout << "part one: " << partition(arr, n, 3) << endl;
    cout << "part two: " << partition(arr, n, 4) << endl;
    return 0;
}

