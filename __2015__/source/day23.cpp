#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <map>
#include <vector>
using namespace std;

int cycle(unsigned int a)
{
    int b = 0;
    while(1){
        if(a == 1){
            return b;
        }
        else{
            b++;
            if(a % 2 == 0){
                a /= 2;
            }
            else{
                a = a * 3 +1;
            }
        }
    }
}

int solve_part(vector<string>& commands, map<string, unsigned int> registers)
{
    unsigned int i = 0;
    regex e("([a-z]{3}).*");
    smatch m;
    while(i < commands.size())
    {
        string cmd = commands.at(i);
        smatch m1;
        regex_match(cmd, m, e);

        if(m[1] == "hlf"){
            regex e1("hlf ([a-z])");
            regex_match(cmd, m1, e1);
            registers.at(m1[1]) /= 2;
        }
        else if(m[1] == "tpl"){
            regex e1("tpl ([a-z])");
            regex_match(cmd, m1, e1);
            registers.at(m1[1]) *= 3;
        }
        else if(m[1] == "inc"){
            regex e1("inc ([a-z])");
            regex_match(cmd, m1, e1);
            registers.at(m1[1]) += 1;
        }
        else if(m[1] == "jmp"){
            regex e1("jmp ([+-]\\d+)");
            regex_match(cmd, m1, e1);
            i += stoi(m1[1]) - 1;
        }
        else if(m[1] == "jie"){
            regex e1("jie ([a-z]), ([+-]\\d+)");
            regex_match(cmd, m1, e1);
            if(registers.at(m1[1]) % 2 == 0){
                i += stoi(m1[2]) - 1;
            }
        }
        else if(m[1] == "jio"){
            regex e1("jio ([a-z]), ([+-]\\d+)");
            regex_match(cmd, m1, e1);
            if(registers.at(m1[1]) == 1){
                i += stoi(m1[2]) - 1;
            }
        }
        else{
            cerr << "Wrong instruction read" << endl;
            return -2;
        }
        i++;
    }
    return registers.at("b");
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    vector<string> commands;
    map<string, unsigned int> registers = {{"a", 0}, {"b", 0}};
    string line;
    while(getline(file, line)){
        commands.push_back(line);
    }
    file.close();
    /*
    cout << "part one: " << day23::solve_part(commands, registers) << endl;
    registers.at("a") = 1;
    cout << "part two: " << day23::solve_part(commands, registers) << endl;
    */
    //values calulting from input 'inc' and 'tpl' commands at beginning
    cout << "part one: " << cycle(4591) << endl;
    cout << "part two: " << cycle(113383) << endl;
    return 0;
}

