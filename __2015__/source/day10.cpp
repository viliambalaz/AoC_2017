#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <sstream>
#define CONWAY 1.30357726903429639125709911215255189
using namespace std;

string look_n_say(string s)
{
    regex e("((\\d)\\2*)");
    smatch m;
    stringstream ss;
    while(regex_search(s, m, e)){
        ss << m[1].length() << m[2];
        s = m.suffix().str();
    }
    return ss.str();
}

int solve_part(string s, int n)
{
    for(int i = 0; i < n; i++)
        s = look_n_say(s);
    return s.length();
}

int growth(string s, int n)
{
    //https://en.wikipedia.org/wiki/Look-and-say_sequence
    return round(s.length() * pow(CONWAY, n));
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string input;
    file >> input;
    file.close();
    cout << "part one: " << solve_part(input, 40) << endl;
    cout << "part two: " << solve_part(input, 50) << endl;
    return 0;
}
