#include <iostream>
#include <fstream>
#include <set>
using namespace std;

int paper(int l, int w, int h)
{
    multiset<int> tmp = {l, w, h};
    int area = 2 * (l*w + l*h + w*h);
    int extra = *tmp.begin() * (*next(tmp.begin()));
    return area + extra;
}

int ribbon(int l, int w, int h)
{
    multiset<int> tmp = {l, w, h};
    int volume = l * w * h;
    int extra = 2 * (*tmp.begin() + (*next(tmp.begin())));
    return volume + extra;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    char c;
    int l, w, h;
    int square_feet = 0, feet =0;
    while(file >> l >> c >> w >> c >> h){
        square_feet += paper(l, w, h);
        feet += ribbon(l, w, h);
    }
    file.close();
    cout << "part one: " << square_feet << endl;
    cout << "part two: " << feet << endl;
    return 0;
}
