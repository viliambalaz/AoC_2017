#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <map>
#include <vector>
#include <set>
using namespace std;

int solve_part(ifstream& file, bool part_2)
{
    map<pair<string, string>, int >happiness;
    set<string> guests;
    string line;
    regex e("(\\w+) would (gain|lose) (\\d+) happiness units by sitting next to (\\w+).");
    smatch m;
    while(getline(file, line))
    {
        regex_match(line, m, e);
        int sign = m[2] == "gain" ? 1 : -1;
        happiness[{m[1],m[4]}] = sign * stoi(m[3]);
        guests.insert(m[1]);
    }
    if(part_2){
        for(string person: guests){
            happiness[{person, "me"}] = 0;
            happiness[{"me", person}] = 0;
        }
        guests.insert("me");
    }
    int max_happy = 0;
    vector<string> seating = vector<string>(guests.begin(), guests.end());
    do
    {
        int happy = 0;
        int n = seating.size();
        for(int i = 0; i < n; i++)
        {
            string left  = seating.at((i+n-1)%n); //positive modulo
            string right = seating.at((i+n+1)%n);
            happy += happiness[{seating.at(i%n),left}] + happiness[{seating.at(i%n), right}];
        }
        if(happy > max_happy){
            max_happy = happy;
        }
    }while(next_permutation(seating.begin(), seating.end()) );
    return max_happy;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    cout << "part_one: " << solve_part(file, false) << endl;
    file.clear();
    file.seekg(0);
    cout << "part_two: " << solve_part(file, true) << endl;
    file.close();
    return 0;
}
