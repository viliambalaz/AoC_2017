#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <map>
using namespace std;

unsigned short wire_value(string w, map<string, string>& commands, map<string, unsigned short>& wires)
{
    smatch m;
    unsigned short tmp;
    if(regex_match(w, m, regex("\\d+")))
        return stoi(w);
    if(wires.at(w) != 0) return wires.at(w);
    else
    {
        string cmd = commands.at(w);
        regex e1("([0-9]+) -> ([a-z]+)");
        regex e2("([a-z]+) -> ([a-z]+)");
        regex e3("(\\w+) AND ([a-z]+) -> ([a-z]+)"); // 1 AND xx
        regex e4("([a-z]+) OR ([a-z]+) -> ([a-z]+)");
        regex e5("([a-z]+) LSHIFT ([0-9]+) -> ([a-z]+)");
        regex e6("([a-z]+) RSHIFT ([0-9]+) -> ([a-z]+)");
        regex e7("NOT ([a-z]+) -> ([a-z]+)");
        if(regex_match(cmd, m, e1)){
            tmp = stoi(m[1]);
            wires.at(m[2]) = tmp;
            return tmp;
        }
        else if(regex_match(cmd, m, e2)){
            tmp = wire_value(m[1], commands, wires);
            wires.at(m[2]) = tmp;
            return tmp;
        }
        else if(regex_match(cmd, m, e3)){
            tmp = wire_value(m[1], commands, wires) & wire_value(m[2], commands, wires);
            wires.at(m[3]) = tmp;
            return tmp;
        }
        else if(regex_match(cmd, m, e4)){
            tmp = wire_value(m[1], commands, wires) | wire_value(m[2], commands, wires);
            wires.at(m[3]) = tmp;
            return tmp;
        }
        else if(regex_match(cmd, m, e5)){
            tmp = wire_value(m[1], commands, wires) << stoi(m[2]);
            wires.at(m[3]) = tmp;
            return tmp;
        }
        else if(regex_match(cmd, m, e6)){
            tmp = wire_value(m[1], commands, wires) >> stoi(m[2]);
            wires.at(m[3]) = tmp;
            return tmp;
        }
        else if(regex_match(cmd, m, e7)){
            tmp = ~wire_value(m[1], commands, wires);
            wires.at(m[2]) = tmp;
            return tmp;
        }
        else{
            cout << "Wrong command: " << cmd << endl;
            throw -3;
        }
    }
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    map<string, unsigned short> wires;
    map<string, string> commands;
    string line;
    regex e(".* -> ([a-z]+)");
    smatch m;
    while(getline(file, line)){
        regex_match(line, m, e);
        wires[m[1]] = 0;
        commands[m[1]] = line;
    }
    file.close();
    string w = "a";
    unsigned short part_1 = wire_value(w, commands, wires);

    for(pair<string, unsigned short> p: wires){
        wires[p.first] = 0;
    }
    wires["b"] = part_1;
    unsigned short part_2 = wire_value(w, commands, wires);

    cout << "part one: " << part_1 << endl;
    cout << "part two: " << part_2 << endl;
    return 0;
}
