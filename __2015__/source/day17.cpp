#include <iostream>
#include <fstream>
#include <list>
using namespace std;

void subset_sum_recursive(list<int> numbers, int target, list<int> partial, list<list<int> >& results)
{
    int s = 0;
    for (list<int>::const_iterator cit = partial.begin(); cit != partial.end(); cit++)
    {
        s += *cit;
    }
    if(s == target)
    {
        results.push_back(partial);
    }
    else if(s >= target)
        return;
    int n;
    for (list<int>::const_iterator ai = numbers.begin(); ai != numbers.end(); ai++)
    {
        n = *ai;
        list<int> remaining;
        for(list<int>::const_iterator aj = ai; aj != numbers.end(); aj++)
        {
            if(aj == ai)continue;
            remaining.push_back(*aj);
        }
        list<int> partial_rec=partial;
        partial_rec.push_back(n);
        subset_sum_recursive(remaining,target,partial_rec, results);
    }
}

list<list<int> > subset_sum(list<int> numbers,int target)
{
    list<list<int> > combi;
    subset_sum_recursive(numbers, target, list<int>(), combi);
    return combi;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }

    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    list<int> containers;
    int x;
    while(file >> x){
        containers.push_back(x);
    }
    file.close();
    list<list<int> > combi = subset_sum(containers, 150);
    int n_containers = 100;
    int n_ways = 0;
    for(list<int> lst : combi)
    {
        if((int)lst.size() < n_containers){
            n_containers = lst.size();
            n_ways = 1;
        }
        else if((int)lst.size() == n_containers){
            n_ways++;
        }
    }
    cout << "part one: " << combi.size() << endl;
    cout << "part two: " << n_ways << endl;
    return 0;
}
