'''
Advent of Code 2015 day 22
python 2.7
'''
import sys

player= {'hitpoints':50, 'attack':0, 'defence':0, 'mana':500}
boss={'hitpoints':55, 'attack':8, 'defence':0, 'mana':0}

spell_names = ['missile', 'drain', 'shield', 'poison', 'recharge']
print spell_names

spells={'missile':[53,1,4,0,0,0],
        'drain':[73,1,2,2,0,0],
        'shield':[113,6,0,0,7,0],
        'poison':[173,6,3,0,0,0],
        'recharge':[229,5,0,0,0,101]}

pasive={'missile':0, 'drain':0, 'shield':0, 'poison':0, 'recharge':0} #turns of pasive
total_spend_mana = 0

def turn(part_2=False):
    global total_spend_mana

    #part two:
    if part_2:
        player['hitpoints'] -= 1

    #print situation
    print 'Player:', player
    print 'BOSS:', boss, '\n'

    if player['mana'] < 53:  #lewest mana required
        print "Player lose all his mana, player lose."
        sys.exit()

    if player['hitpoints'] <= 0:
        print "BOSS kill the player, player lose."
        sys.exit()

    while 1:
        spell = raw_input('cast spell')
        if spell not in spell_names:
            print 'wrong input'
            continue

        if spells[spell][0] > player['mana']:#not enough mana
            print "not enough mana"
            spell = 0
            continue

        if pasive[spell] > 0:#already charged
            print 'Spell already charged.'
            spell = 0
            continue
        break

    pasive[spell] = spells[spell][1]
    player['mana'] -= spells[spell][0]
    total_spend_mana += spells[spell][0]
    print pasive

    for pas in pasive:
        if pasive[pas] > 0:
            pasive[pas] -= 1 #decrement turns of pasive
            boss['hitpoints'] -= spells[pas][2]
            player['hitpoints'] += spells[pas][3]
            player['mana'] += spells[pas][5]
    #specific for spell shield
    if pasive['shield'] > 0:
        player['defence'] = spells['shield'][4]
    else: 
        player['defence']=0

    #--------------------boss_turn-----------------
    #print situation
    print 'Player:', player
    print 'BOSS:', boss, '\n'

    if boss['hitpoints'] <= 0:
        print "You kill the BOSS, Player wins."
        print "totalspend mana:", total_spend_mana
        sys.exit()

    print pasive
    #activate passive
    for pas in pasive:
        if pasive[pas] > 0:
            pasive[pas] -= 1 #decrement turns of pasive
            boss['hitpoints'] -= spells[pas][2]
            player['hitpoints'] += spells[pas][3]
            player['mana'] += spells[pas][5]


    #boss attack
    if player['defence'] >= boss['attack']:
        damage = 1
    else:
        damage = boss['attack'] - player['defence']
    player['hitpoints'] -= damage

    #specific for spell shield
    if pasive['shield'] > 0:
        player['defence'] = spells['shield'][4]
    else: 
        player['defence'] = 0

    if boss['hitpoints'] <= 0:
        print "You kill the BOSS, Player wins."
        print "totalspend mana:", total_spend_mana
        sys.exit()

if __name__ == '__main__':
    inp = raw_input("part 2? [y/n]")
    part_2 = True if inp == 'y' else Falses
    for i in range(20):
        turn(part_2)

