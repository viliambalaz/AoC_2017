# Wizard Simulator
Simulation of the game Wizard Simulator 20XX [Day 18](http://adventofcode.com/2015/day/18).
## Testcase
- Hit Points: 55 Damage: 8
- part one: poison, recharge, shield, poison, 5x missile, mana spend: 953
- part two: poison, recharge, shield, poison, recharge, drain, poison, drain, missile, mana spend: 1289
