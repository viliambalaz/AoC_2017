#python 2.7.12

from Tkinter import *
from PIL import Image, ImageTk
from tkFileDialog import askopenfilename, asksaveasfilename
import re, os
root = Tk()


def image(path, height=10, width=10):
    pixel = Image.open(path)
    pixel = pixel.resize((height, width), Image.ANTIALIAS)
    pixel = ImageTk.PhotoImage(pixel)
    return pixel


class Application(object):
    'mutual variables for both classes LED and LED_grid'
    lights = set()
    plights = set() # LED, that are always on
    running = False
    
    path = os.path.dirname(os.path.realpath(__file__))
    lights_img = image(path + "/black.png")
    bg_img = image(path + "/white.jpg")
    plights_img = image(path + "/blue.jpg")


#LED Button
class LED(Button):

    def __init__(self, parent, x, y, **kw):
        Button.__init__(self, parent, kw, width=10, height=10)
        self.bind('<Button-1>', self.light_control)
        self.bind('<Button-3>', self.plight_control)
        self.state = 0
        self.x = x
        self.y = y

    def light_control(self, event):
        'user-choice lights'
        if not Application.running:
            if self.state == 1:
                Application.lights.remove((self.x, self.y))
                self.config(image=Application.bg_img)
                self.state = 0
            elif self.state == 0:
                Application.lights.add((self.x, self.y))
                self.config(image=Application.lights_img)
                self.state = 1

    def plight_control(self, event):
        'user-choice p-lights'
        if not Application.running:
            if self.state == 2:
                Application.plights.remove((self.x, self.y))
                self.config(image=Application.bg_img)
                self.state = 0
            elif self.state == 0:
                Application.plights.add((self.x, self.y))
                self.config(image=Application.plights_img)
                self.state = 2


class LED_grid(Frame):
    def __init__(self, row, col, grid_borders=True, bgcolor=True, parent=None, **kw):
        Frame.__init__(self, root, kw)
        self.row = row
        self.col = col

        self.load_lights = set() # variables for function load_grid
        self.load_plights = set()

        self.canvas = Canvas(self, borderwidth=0, background="#ffffff")
        self.cvs_frame = Frame(self.canvas, background="#ffffff")
        self.vsb = Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.hsb = Scrollbar(self, orient="horizontal", command=self.canvas.xview)
        self.canvas.configure(yscrollcommand=self.vsb.set)
        self.canvas.configure(xscrollcommand=self.hsb.set)

        self.vsb.pack(side="right", fill="y")
        self.hsb.pack(side="bottom", fill="x")
        self.canvas.pack(side="left", fill="both", expand=True)
        self.cvs_frame.pack()
        self.canvas.create_window((0, 0), window=self.cvs_frame, anchor="nw", tags="self.cvs_frame")

        self.cvs_frame.bind("<Configure>", self.onFrameConfigure)

        self.filename = ''

        # grid_borders: True => grid has borders; False => grid is continuous
        self.grid_borders = grid_borders
        if self.grid_borders:
            self.neighbours = lambda x, y:sum((i, j) in (Application.lights | Application.plights)
                                              for i in (x - 1, x, x + 1) for j in (y - 1, y, y + 1) if (i, j) != (x, y))
        else:
            self.neighbours = lambda x, y:sum((i % self.row, j % self.col) in (Application.lights | Application.plights)
                                              for i in (x - 1, x, x + 1) for j in (y - 1, y, y + 1) if (i, j) != (x, y))

        if bgcolor:
            Application.bg_img = image(Application.path + "/white.jpg")
            Application.lights_img = image(Application.path + "/black.png")
        else:
            Application.bg_img = image(Application.path + "/black.png")
            Application.lights_img = image(Application.path + "/white.jpg")

        self.field = [[None for j in range(col)] for i in range(row)]
        self.create_grid()
        # ___________________init_________________________________

    def onFrameConfigure(self, event):
        'Reset the scroll region to encompass the inner frame'
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def create_grid(self):
        for i in range(self.row):
            line = Frame(self.cvs_frame)
            for j in range(self.col):
                led = LED(line, i, j, image=Application.bg_img)
                led.pack(side=LEFT, padx=0, pady=0)
                self.field[i][j] = led
            line.pack()

    def load_grid(self):
        #self.clear_work()
        self.stop_work()
        Application.lights = set(self.load_lights)
        Application.plights = set(self.load_plights)
        self.set_state()
        self.display_lights()

    def turn(self):
        Application.lights = {(x, y) for x in xrange(self.row) for y in xrange(self.col)
                              if (x, y) in Application.lights and 2 <= self.neighbours(x, y) <= 3
                              or (x, y) not in Application.lights and self.neighbours(x, y) == 3}
        Application.lights -= Application.plights

    def set_state(self):
        for i in range(self.row):
            for j in range(self.col):
                self.field[i][j].state = 0
        for (i,j) in Application.lights:
            self.field[i][j].state = 1
        for (i,j) in Application.plights:
            self.field[i][j].state = 2

    def display_lights(self):
        for i in range(self.row):
            for j in range(self.col):
                if self.field[i][j].state == 1:
                    self.field[i][j].config(image=Application.lights_img)
                elif self.field[i][j].state == 2:
                    self.field[i][j].config(image=Application.plights_img)
                else:
                    self.field[i][j].config(image=Application.bg_img)

    def do_work(self):
        self.turn()
        self.set_state()
        self.display_lights()

    def update(self):
        self.do_work()
        self._update = self.after(300, self.update)

    def start_work(self):
        if not Application.running:
            print("running...")
            self.update()
            Application.running = True

    def stop_work(self):
        if Application.running:
            print("stopped...")
            self.after_cancel(self._update)
            Application.running = False

    def clear_work(self):
        self.stop_work()
        Application.lights.clear()
        Application.plights.clear()
        for i in range(self.row):
            for j in range(self.col):
                self.field[i][j].state = 0
                self.field[i][j].config(image=Application.bg_img)

    def reset_work(self):
        #self.clear_work()
        self.load_grid()

    def save_work(self):
        """ temporally save file only for reset """
        running = Application.running
        self.stop_work() # stop, to grid do not change
        self.load_lights.clear()
        self.load_plights.clear()
        for i in range(self.row):
            for j in range(self.col):
                if self.field[i][j].state == 1:
                    self.load_lights.add((i, j))
                elif self.field[i][j].state == 2:
                    self.load_plights.add((i, j))
        print "configuration temporally saved"
        if running:
            self.start_work()

    def load_work(self):
        self.stop_work()
        Tk().withdraw()
        filename = askopenfilename()
        if filename:  # user choice file
            if self.valid_file(filename):
                self.filename = filename
                print "opened file: ", filename
                self.load_grid()

    def saveas_work(self):
        'save current grid into text file'
        self.stop_work()
        Tk().withdraw()
        filename = asksaveasfilename()
        if filename:
            fp = open(filename, 'w')
            for i, j in Application.lights:
                fp.write("l {},{}\n".format(i, j))
            for i, j in Application.plights:
                fp.write("p {},{}\n".format(i, j))
            fp.close()
            print "configuration saved"

    def settings_work(self):
        self.stop_work()
        self.newWindow = Toplevel(self.master)
        self.app = Settings(self.newWindow, self)
        self.app.pack()

    def valid_file(self, filename):
        """
            control if opened file has valid format, if so, it store configuration
            match format: 'x 12,23'; x -> {l, p} l - light, p -p_light; only positive numbers
        """
        pattern = r'^([p|l]) (\d+),(\d+)$'
        tmp_lights = set()
        tmp_plights = set()

        with open(filename) as fp:
            for line in fp:
                s = line.strip()
                m = re.match(pattern, s)
                if m:
                    x, y = int(m.group(2)), int(m.group(3))
                    if x >= self.row or y >= self.col:
                        print "out of LED_grid: ({}, {})".format(x, y)
                        return False
                    if m.group(1) == 'l':
                        tmp_lights.add((int(m.group(2)), int(m.group(3))))
                    elif m.group(1) == 'p':
                        tmp_plights.add((int(m.group(2)), int(m.group(3))))
                else:
                    print "invalid format"
                    return False

        self.load_lights = set(tmp_lights)
        self.load_plights = set(tmp_plights)
        return True

    def move_grid(self, x, y):
        ' move current grid configuration up/down/left/right '
        if not Application.running:

            if self.grid_borders:
                Application.lights = {(i + x, j + y) for i, j in Application.lights
                                      if 0 <= (i + x) < self.row and 0 <= (j + y) < self.col}
                Application.plights = {(i + x, j + y) for i, j in Application.plights
                                      if 0 <= (i + x) < self.row and 0 <= (j + y) < self.col}
            else:
                Application.lights = {((i + x) % self.row, (j + y) % self.col) for i, j in Application.lights}
                Application.plights = {((i + x) % self.row, (j + y) % self.col) for i, j in Application.plights}

        self.set_state()
        self.display_lights()

    def leftarrow_work(self, event=None):
        self.move_grid(0, -1)

    def rightarrow_work(self, event=None):
        self.move_grid(0, 1)

    def uparrow_work(self, event=None):
        self.move_grid(-1, 0)

    def downarrow_work(self, event=None):
        self.move_grid(1, 0)

    def reconfigure(self, x, y, grid_borders=True, bgcolor=True):
        self.__init__(x, y, grid_borders=grid_borders, bgcolor=bgcolor, parent=root)
        self.pack(side="top", fill="both", expand=True)


class Settings(Frame):

    def __init__(self, parent, LED_app, **kw):
        Frame.__init__(self, parent, LED_app)

        self.LED_app = LED_app
        grid_size = Frame(self)
        lbl_size = Label(grid_size, text="grid_size")
        self.x_size = Widget_plusminus(grid_size)
        self.y_size = Widget_plusminus(grid_size)

        lbl_size.pack(side=LEFT)
        self.x_size.pack(side=LEFT, padx=10)
        self.y_size.pack(side=LEFT, padx=10)
        grid_size.pack()

        self.borders_return = IntVar(value=1)
        self.bgcolor_return = IntVar(value=1)
        infgrid = self.options_frame(self.borders_return, "grid borders", "yes", "no")
        bgcolor = self.options_frame(self.bgcolor_return, "background", "white", "black")

        infgrid.pack()
        bgcolor.pack()

        buttons_frame = Frame(self)
        save_button = Button(buttons_frame, text='Save', command=self.save_work)
        cancel_button = Button(buttons_frame, text='Cancel', command=self.master.destroy)
        save_button.pack(side=RIGHT)
        cancel_button.pack(side=RIGHT)
        buttons_frame.pack(side=RIGHT)

    def options_frame(self, ivar, lbl_text, rd1_text='', rd2_text=''):
        frm = Frame(self)
        lbl = Label(frm, text=lbl_text)
        rd1 = Radiobutton(frm, text=rd1_text, variable=ivar, value=1)
        rd2 = Radiobutton(frm, text=rd2_text, variable=ivar, value=0)
        lbl.pack(side=LEFT)
        rd1.pack(side=LEFT, padx=10)
        rd2.pack(side=LEFT, padx=10)
        return frm

    def save_work(self):
        'save new configurations'
        row = int(self.x_size.lbl.cget('text'))
        col = int(self.y_size.lbl.cget('text'))
        borders = int(self.borders_return.get())
        bgcolor = int(self.bgcolor_return.get())
        print row, col, borders, bgcolor
        self.master.destroy()
        self.LED_app.reconfigure(row, col, grid_borders=borders, bgcolor=bgcolor)

class Widget_plusminus(Frame):
    """
        99 |+||-|
    """
    def __init__(self, parent, **kw):
        Frame.__init__(self, parent, kw)

        self.limit_d = 3
        self.limit_u = 100
        self.lbl = Label(self, text=str(self.limit_d))

        self.is_pressed = False
        plus_button = Button(self, text='+')
        plus_button.bind('<Button-1>', self.click)
        plus_button.bind('<ButtonRelease-1>', self.release)

        minus_button = Button(self, text='-')
        minus_button.bind('<Button-1>', self.click)
        minus_button.bind('<ButtonRelease-1>', self.release)

        self.lbl.pack(side=LEFT)
        plus_button.pack(side=LEFT)
        minus_button.pack(side=LEFT)

    def click(self, event):
        self.is_pressed = True
        self.inc = 1 if event.widget.cget('text') == '+' else -1
        self.pool()

    def release(self, event):
        self.after_cancel(self.after_id)

    def pool(self):
        if self.is_pressed:
            self.work()
            self.after_id = self.after(100, self.pool)

    def work(self):
        value = int(self.lbl.cget('text')) + self.inc
        if value > self.limit_u or value < self.limit_d:
            value -= self.inc
        self.lbl.config(text=value)


def run(x=30, y=30):
    print __name__
    app = LED_grid(x, y, parent=root, grid_borders=False)

    mainMenu = Menu(root)
    app.master.config(menu=mainMenu)

    fileMenu = Menu(mainMenu)
    mainMenu.add_cascade(label='File', menu=fileMenu)
    fileMenu.add_command(label='New', command=app.clear_work)
    fileMenu.add_command(label='Load', command=app.load_work)
    fileMenu.add_command(label='Save as', command=app.saveas_work)
    fileMenu.add_command(label='Settings', command=app.settings_work)
    fileMenu.add_command(label='Quit', command=app.quit)

    toolbar = Frame(root)
    do_button = Button(toolbar, text="DO", bg="blue", command=app.do_work)
    start_button = Button(toolbar, text="Start", bg="green", command=app.start_work)
    stop_button = Button(toolbar, text="Stop", bg="red", command=app.stop_work)
    reset_button = Button(toolbar, text="Reset", command=app.reset_work)
    clear_button = Button(toolbar, text="Clear", command=app.clear_work)
    save_button = Button(toolbar, text="Save", command=app.save_work)
    do_button.pack(side=LEFT)
    start_button.pack(side=LEFT)
    stop_button.pack(side=LEFT)
    reset_button.pack(side=LEFT)
    clear_button.pack(side=LEFT)
    save_button.pack(side=LEFT)
    toolbar.pack()

    arrows = Frame(root)
    arrows.bind('<Left>', app.leftarrow_work)
    arrows.bind('<Right>', app.rightarrow_work)
    arrows.bind('<Up>', app.uparrow_work)
    arrows.bind('<Down>', app.downarrow_work)
    arrows.focus_set()
    arrows.pack(side="bottom")

    app.pack(side="top", fill="both", expand=True)
    root.mainloop()

run()
