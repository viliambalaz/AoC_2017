# Conway's Game of Life
Visualization for  [Day 18](http://adventofcode.com/2015/day/18): Like a GIF For Your Yard. More info on [wiki](https://en.wikipedia.org/wiki/Conway's_Game_of_Life).

## Dependencies
- python 2.7.12
- used libraries: Tkinter, PIL, tkFileDialog, re, os

## Controls
- Add/Remove light by left-clicking on the square.
- Add/Remove permanent light by right-clicking on the square.
