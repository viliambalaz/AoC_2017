'''
Advent of Code 2015 day 06
python 2.7
'''
from Tkinter import *
from PIL import Image, ImageTk
import re, os

print sys.argv
fp = open(sys.argv[1], 'r')
pattern = r"([a-zA-Z ]*)(\d*),(\d*) through (\d*),(\d*)"
instructions = []
with fp:
    for line in fp:
        inst = re.match(pattern, line)
        op = inst.groups()[0].strip()
        x1, y1, x2, y2 = map(int, inst.groups()[1:])
        instructions.append([op, x1, y1, x2, y2])
        #print op, x1, y1, x2, y2


#LED Button
class LED(Label):

    def __init__(self, parent=None, **kw):
        Label.__init__(self, parent, kw, bg='black', height=1, width=1)
        self.status = 0


#create application
class LED_grid(Frame):
    def __init__(self, row, col, instructions, master=None, **kw):
        Frame.__init__(self, master, kw)
        self.row = row
        self.col = col
        self.instructions = instructions
        self.no_i = len(instructions)
        self.i = 0

        self.field = [[None for j in range(row)] for i in range(col)]
        path = os.path.dirname(os.path.realpath(__file__))
        self.black = self.image(path + "/black.png")
        self.white = self.image(path + "/white.jpg")
        self.create_grid()

    def image(self, path, height=3, width=3):
        pixel = Image.open(path)
        pixel = pixel.resize((height, width), Image.ANTIALIAS)
        pixel = ImageTk.PhotoImage(pixel)
        return pixel

    def create_grid(self):
        for i in range(self.row):
            line = Frame(self)
            for j in range(self.col):
                led = LED(line, image=self.black)
                led.pack(side=LEFT)
                self.field[i][j] = led
            line.pack()

    def read_inst(self):
        if self.i >= self.no_i:
            self.i = 0

        print(self.i, self.instructions[self.i])
        op, x1, y1, x2, y2 = self.instructions[self.i]
        x1/=10; y1/=10; x2/=10; y2/=10
        self.i += 1
        if op == "turn on":
            for i in range(x1,x2 + 1):
                for j in range(y1, y2 + 1):
                    self.field[i][j].config(image=self.white)
                    self.field[i][j].status = 1
        elif op == "turn off":
            for i in range(x1, x2 + 1):
                for j in range(y1, y2 + 1):
                    self.field[i][j].config(image=self.black)
                    self.field[i][j].status = 0
        elif op == "toggle":
            for i in range(x1, x2 + 1):
                for j in range(y1, y2 + 1):
                    if self.field[i][j].status == 0:
                        self.field[i][j].config(image=self.white)
                        self.field[i][j].status = 1
                    elif self.field[i][j].status == 1:
                        self.field[i][j].config(image=self.black)
                        self.field[i][j].status = 0

    def update(self):
        self.read_inst()
        self._update = self.after(100, self.update)

    def start_work(self):
        self.update()

    def stop_work(self):
        self.after_cancel(self._update)


def run():
    root = Tk()
    app = LED_grid(100, 100, instructions, master=root)

    menu = Frame(root)
    do_button = Button(menu, text="DO", bg="blue", command=app.read_inst)
    start_button = Button(menu, text="Start", bg="green", command=app.start_work)
    stop_button = Button(menu, text="Stop", bg="red", command=app.stop_work)
    do_button.pack(side=LEFT)
    start_button.pack(side=LEFT)
    stop_button.pack(side=LEFT)

    menu.pack()
    app.pack()
    root.mainloop()

run()
