# Year 2015
All solutions written in C++. Day22 as simulator in bonus.
- `g++ -std=gnu++11 day^^.cpp`
- `./a.out input_file`
- day04 link with `-lcrypto`
## Bonus
Written in python, version 2.7.12.
- day06: Fire Hazard
- day18: Conways Game of Life
- day20: Wizard Simulator

## Execution time
- day01 - 0.00s
- day02 - 0.00s
- day03 - 0.01s
- day04 - 7.67s
- day05 - 0.61s
- day06 - 0.21s
- day07 - 3.60s
- day08 - 0.00s
- day09 - 0.22s
- day10 - too long for part 2
- day11 - 29.26s
- day12 - 0.02s
- day13 - 5.65s
- day14 - 0.00s
- day15 - 0.00s
- day16 - 0.01s
- day17 - 0.11s
- day18 - 3.16s
- day19 - 0.07s
- day20 - 0.74s
- day21 - 0.00s
- day22 - -.--s
- day23 - 0.00s
- day24 - 0.00s
- day25 - 0.36s

