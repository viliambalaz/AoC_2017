#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <algorithm>
#include <set>
#include <regex>
#include <numeric>
using namespace std;

int tree_weight(string tower, map<string, vector<string>>& tree, map<string, int>& weight)
{
    vector<int> sub;
    map<int, int> balanced;
    map<int, string> mp;
    for(string c: tree[tower])
    {
        int w = tree_weight(c, tree, weight);
        sub.push_back(w);
        mp[w] = c;
        if(balanced.find(w) == balanced.end())
            balanced[w] = 1;
        else
            balanced[w]++;
    }
    if(balanced.size() == 2)
    {
        map<int, int>:: iterator target = balanced.begin();
        map<int, int>:: iterator failure = next(balanced.begin());

        if((*target).second < (*failure).second)
            swap(target, failure);
        cout << "part two: " << weight[mp[(*failure).first]] + (*target).first - (*failure).first << endl;
        throw 0;
    }
    return weight[tower] + accumulate(sub.begin(), sub.end(), 0);
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string line;
    smatch m;
    regex e ("\\w+");
    map<string, int> weight;
    map<string, vector<string>> tree;
    set<string> parents;
    set<string> children;

    while(getline(file,line))
    {
        stringstream ss;
        string tower, sub_tower;
        int w;

        while (std::regex_search(line,m,e)) {
            ss << *(m.begin()) << endl;
            line = m.suffix().str();
        }
        ss >> tower;
        ss >> w;
        weight[tower] = w;
        tree[tower] = {};
        parents.insert(tower);
        while(ss >> sub_tower){
            tree[tower].push_back(sub_tower);
            children.insert(sub_tower);
        }
    }
    string root;
    vector<string> diff(1);
    set_difference(parents.begin(), parents.end(), children.begin(), children.end(), diff.begin());
    root = *diff.begin();
    cout << "part one: " << root << endl;

    try{
    tree_weight(root, tree, weight);
    }
    catch(int a){;}
    file.close();
    return 0;
}

