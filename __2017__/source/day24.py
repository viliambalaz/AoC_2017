import re, sys


def next_port(bridge, ports, rm_pt, valid_bridges):
    ports = ports[:]
    ports.remove(rm_pt)
    last = bridge[-1]
    for pt in ports:
        p1, p2 = pt
        if p1 == last[1]:
            next_port(bridge + [(p1, p2)], ports, pt, valid_bridges)
        elif p2 == last[1]:
            next_port(bridge + [(p2, p1)], ports, pt, valid_bridges)   
    valid_bridges.append(bridge)


def score_bridge(bridge):
    power = 0
    for p1, p2 in bridge:
        power += p1 + p2
    return power


def main():
    if len(sys.argv) != 2:
       print("Wrong arguments")
       return -1

    components = {}
    pattern = r'(\d+)/(\d+)'
    ports = [(0,0)]
    ix = 0
    valid_bridges = []
    with open(sys.argv[1]) as fp:
        for line in fp:
            m = re.match(pattern, line)
            pin_1, pin_2 = map(int, m.groups())
            ports.append((pin_1, pin_2))

    bridge = [(0,0)]
    next_port(bridge, ports, (0,0), valid_bridges)
    
    strongest = max(map(score_bridge, valid_bridges))
    print("part one:", strongest)

    longest = max(map(len, valid_bridges))
    strongest_longest = max(map(score_bridge, filter(lambda bridge: len(bridge)==longest, valid_bridges)))
    print("part two:", strongest_longest)


if __name__ == "__main__":
    main()
