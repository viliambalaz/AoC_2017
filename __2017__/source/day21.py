import numpy as np
import re, sys


def blockshaped(arr, nrows, ncols):
    h, w = arr.shape
    return (arr.reshape(h//nrows, nrows, -1, ncols)
               .swapaxes(1,2)
               .reshape(-1, nrows, ncols))


def unblockshaped(arr, h, w):
    n, nrows, ncols = arr.shape
    return (arr.reshape(h//nrows, -1, nrows, ncols)
               .swapaxes(1,2)
               .reshape(h, w))


def get_rule(arr, c='/'):
    first_join = list()
    for x in arr:
        first_join.append(''.join(x))
    return c.join(first_join)


def make_matrix(seq):
    matrix = seq.split('/')
    matrix = [list(row) for row in matrix]
    return np.array(matrix)


def solve(image, rules, loop):
    image = image[:]
    for _ in range(loop):
        size = len(image)
        if size % 2 == 0:
            blocks = list(blockshaped(image, 2, 2))
            squares = size / 2
        elif size % 3 == 0:
            blocks = list(blockshaped(image, 3, 3))
            squares = size / 3
  
        for i in range(len(blocks)):
            blocks[i] = rules[get_rule(blocks[i])]    
        image = unblockshaped(np.array(blocks), size + squares, size + squares)
  
    return sum([list(row).count('#') for row in image])


def main():
    if(len(sys.argv) != 2):
        print("Wrong arguments")
        return -1
    
    image = np.array([['.','#','.'],['.','.','#'],['#','#','#']])
    rules = {}
    pattern = r"(\S+) => (\S+)"
    with open(sys.argv[1]) as fp:
        for line in fp:
            m = re.match(pattern, line)
            matrix = make_matrix(m.group(1))
            matricies = []
            for k in [0, 1, 2, 3]:
                rot = np.rot90(matrix, k=k)
                matricies.append(rot)
                matricies.append((np.fliplr(rot)))
                matricies.append((np.flipud(rot)))

            enhacement = make_matrix(m.group(2))
            for mtx in matricies:
                rules[get_rule(mtx)] = enhacement
    

    print("part one:", solve(image, rules, 5))
    print("part one:", solve(image, rules, 18))

if __name__ == "__main__":
    main()

