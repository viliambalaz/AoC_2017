import re, sys


def main():
    if len(sys.argv) != 2:
       print("Wrong arguments")
       return -1
    
    fp = open(sys.argv[1])
    pattern = r'(\d+)'
    rules = {}  # 'A': [(1, 1, B), (0, -1, B)] #(write, move, state)
    state_it = re.search(r'([A-Z])\.', fp.readline()).group(1)
    steps = re.search(r'(\d+)', fp.readline()).group(1)
    
    while(fp.readline()):
        state = re.search(r'([A-Z]):', fp.readline()).group(1)
    
        condi_1 = int(re.search(r'(\d+)', fp.readline()).group(1))
        write_1 = int(re.search(r'(\d+)', fp.readline()).group(1))
        move_1 = 1 if re.search(r'right', fp.readline()) else -1
        state_1 = re.search(r'([A-Z])\.', fp.readline()).group(1)
    
        condi_2 = int(re.search(r'(\d+)', fp.readline()).group(1))
        write_2 = int(re.search(r'(\d+)', fp.readline()).group(1))
        move_2 = 1 if re.search(r'right', fp.readline()) else -1
        state_2 = re.search(r'([A-Z])\.', fp.readline()).group(1)
    
        rules[state] = [(write_1, move_1, state_1), (write_2, move_2, state_2)]
    fp.close()
    
    ix = 0
    config = dict()
    for i in range(int(steps)):
        if ix not in config:
            config[ix] = 0
        if config[ix]: g = 1
        elif config[ix] == 0: g = 0
        else: raise ValueError
    
        config[ix] = rules[state_it][g][0]
        ix += rules[state_it][g][1]
        state_it = rules[state_it][g][2]
    
    print("part one:", sum(config.values()))


if __name__ == "__main__":
    main()
