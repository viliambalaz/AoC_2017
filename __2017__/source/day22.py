import sys

def solve(grid, x, y, loop, part2=False):    
    grid = dict(grid)    
    cardinal = [(-1, 0), (0, 1), (1, 0), (0, -1)]
    direction = 0
    infected = 0
    for i in range(loop):
        if not ((x,y) in grid):
            grid[(x,y)] = '.'

        if grid[(x,y)] == '.':
            direction = (direction - 1) % 4
            grid[(x,y)] = '#'
            if not part2: infected += 1
            if part2: grid[(x,y)] = 'W'    
        elif grid[(x,y)] == '#':
            direction = (direction + 1) % 4
            grid[(x,y)] = '.'
            if part2: grid[(x,y)] = 'F'
        #part two
        elif grid[(x,y)] == 'F':
            direction = (direction + 2) % 4
            grid[(x,y)] = '.'
        elif grid[(x,y)] == 'W':
            grid[(x,y)] = '#'
            infected += 1
    
        x += cardinal[direction][0]
        y += cardinal[direction][1]
    
    return infected
        

def main():

    if len(sys.argv) != 2:
       print("Wrong arguments")
       return -1
        
    grid = {}
    with open(sys.argv[1]) as fp:
        for i, line in enumerate(fp):
            line = line.strip()
            for j, c in enumerate(line):
                if c == '#': grid[(i, j)] = '#'
    x,y = i//2, j//2
    print("part one:", solve(grid, x, y, 10000))
    print("part two:", solve(grid, x, y, 10000000, part2=True))


if __name__ == "__main__":
    main()
