#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

#define LOOP 50000000

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "wrong arguments." << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return  -2;
    }
    int step, pos;
    file >> step;
    file.close();

    vector<int> buff(1,0);
    for(int i=1; i<=2017; i++){
        pos = ((pos + step) % buff.size()) + 1;
        buff.insert(buff.begin() + pos, i);
    }
    cout << "part one: " << *next(find(buff.begin(), buff.end(), 2017)) << endl;

    int res;
    int buff_size = 1;
    buff = vector<int>(1,0);
    for(int i=1; i<=LOOP; i++)
    {
        pos = ((pos + step) % buff_size) + 1;
        buff_size++;
        if(pos == 1)
            res = i;
    }
    cout << "part two: " << res << endl;
    return 0;
}
