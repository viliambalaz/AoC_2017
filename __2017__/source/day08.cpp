#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <map>
#include <functional>
using namespace std;

void command(map<string, function<bool(int, int)> >op, map<string, int>& reg, istringstream is, int& maxi)
{
    string a, plus_minus, fi, b, con; int x,y;
    is >> a >> plus_minus >> x >> fi >> b >> con >> y;
    if(op[con](reg[b],y)){
        plus_minus == "inc" ? reg[a] += x : reg[a] -= x;
        if(reg.at(a) > maxi) maxi = reg[a];
    }
    return;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    int maxi_2 = 0;
    map<string, int> reg;
    map<string, function<bool(int, int)> > op;
    op["=="] = [](int a, int b)->bool {return a == b;};
    op["!="] = [](int a, int b)->bool {return a != b;};
    op[">"] = [](int a, int b)->bool {return a > b;};
    op["<"] = [](int a, int b)->bool {return a < b;};
    op[">="] = [](int a, int b)->bool {return a >= b;};
    op["<="] = [](int a, int b)->bool {return a <= b;};

    string line;
    while(getline(file, line)){
        command(op, reg, istringstream(line), maxi_2);
    }
    int maxi_1 = 0;
    for(pair<string, int> p : reg)
        maxi_1 = max(maxi_1, p.second);

    cout << "part one: " << maxi_1 << endl;
    cout << "part two: " << maxi_2 << endl;

    file.close();
    return 0;
}

