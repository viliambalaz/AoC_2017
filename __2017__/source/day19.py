import sys

def start_pos(line):
    for i in range(len(line)):
        if line[i] == '|':
            return i
    return None

def main():
    
    if len(sys.argv) != 2:
        print("Wrong arguments")
        return -1
    
    fp = open(sys.argv[1])
    data = [list(line.strip('\n')) for line in fp.readlines()]
    fp.close()

    road = str()
    cardinal = [[1,0], [-1,0], [0,1], [0, -1]]
    direction = [1, 0]
    pos = [0, start_pos(data[0])]
    steps = 0
    while(1):
        steps += 1
        i, j = pos
        prev = data[i][j]
        
        if data[i][j].isalpha():
            road += data[i][j]
        
        if data[i][j] == '+':  #change direction
            for x, y in cardinal:
                if (0 <= i+x < len(data)) and (0 <= j+y < len(data[i])) and (data[i+x][j+y] != ' ') and (direction != list(map(lambda n : -n, [x,y]))):
                    direction = [x, y]
                    break
        elif (not(0 <= i+direction[0] < len(data))) or (not(0 <= j+direction[1] < len(data[i]))) or data[i+direction[0]][j+direction[1]] == ' ':
            print ("part one:", road)
            print ("part two:", steps)
            break

        i += direction[0]
        j += direction[1]
        pos = [i, j]

if __name__ == '__main__':
    main()

