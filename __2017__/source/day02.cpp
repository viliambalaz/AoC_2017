#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <climits>
#include <vector>
#include <algorithm>
using namespace std;

int part1(ifstream& file)
{
    file.clear();
    file.seekg(0);
    istringstream sline;
    string line;
    int n, sum = 0;
    int low = INT_MAX, high = -1;

    while(getline(file, line))
    {
        sline =  istringstream(line);
        while(sline >> n){
            if(n < low) low = n;
            if(n > high) high = n;
        }
        sum += high - low;
        low = INT_MAX; high = -1;
    }
    return sum;
}

int part2(ifstream& file)
{
    file.clear();
    file.seekg(0);
    istringstream sline;
    string line;
    int n, sum = 0;

    while(getline(file, line))
    {
        vector<int> numbers;
        sline =  istringstream(line);
        while(sline >> n){
            numbers.push_back(n);
        }
        sort(numbers.begin(), numbers.end(), greater<int>());
        for(unsigned i=0; i < numbers.size(); i++)
        {
            for(unsigned j=i+1; j < numbers.size(); j++)
                if(numbers.at(i) % numbers.at(j) == 0){
                    sum += numbers.at(i) / numbers.at(j);
                    break;
                }
        }
    }
    return sum;
}


int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    cout << "Part 1: " << part1(file) << endl;
    cout << "Part 2: " << part2(file) << endl;

    file.close();
    return 0;
}

