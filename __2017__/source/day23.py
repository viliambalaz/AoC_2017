import sys


def part_1(commands, registers):
    registers = dict(registers)
    i, n = 0, 0
    while(0 <= i < len(commands)):
        cmd = commands[i]    
        if cmd[0] == "set":
            try:registers[cmd[1]] = registers[cmd[2]]
            except KeyError: registers[cmd[1]] = int(cmd[2])
        elif cmd[0] == "sub":
            try:registers[cmd[1]] -= registers[cmd[2]]
            except KeyError: registers[cmd[1]] -= int(cmd[2])
        elif cmd[0] == "mul":
            try:registers[cmd[1]] *= registers[cmd[2]]
            except KeyError: registers[cmd[1]] *= int(cmd[2])
            n+=1
        elif cmd[0] == "jnz":
            try: x = registers[cmd[1]]
            except KeyError: x = int(cmd[1])
            if x: i += int(cmd[2]) -1
        i += 1
    return n
   
 
def part_2(b):
    h = 0
    b = b *100 + 100000
    c = b + 17000
    for x in range(b, c+1, 17):
        for i in range(2, x):
            if x % i == 0:
                h += 1
                break
    return h


def main():
    if len(sys.argv) != 2:
       print("Wrong arguments")
       return -1    
    fp = open(sys.argv[1])
    commands = [line.strip().split() for line in fp.readlines()]
    fp.close()
    
    abc = "abcdefgh"
    registers = dict()
    for c in abc:
        registers[c] = 0
    start_b = int(commands[0][2])
    
    print("part one:", part_1(commands, registers))
    print("part two:", part_2(start_b))

if __name__ == "__main__":
    main()
