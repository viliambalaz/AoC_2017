#include <iostream>
#include <fstream>
#include <map>
#include <vector>
using namespace std;

int scanner_position(int range, int seconds)
{
    /// (10**(i-1) / 9) ** 2
    vector<int> demlo = {0};
    for(int i=1; i< range; i++)
        demlo.push_back(i);
    for(int i=range-2; i>0; i--)
        demlo.push_back(i);
    return demlo[seconds % demlo.size()];
}

int part2(map<int, int> scanners)
{
    for(int i=1; ; i++)
    {
        bool crash = false;
        for(pair<int, int> p : scanners)
        {
            int m = (i + p.first) % (2*(p.second -1));
            if(m == 0){
                crash = true;
                break;
            }
        }
        if(!crash) return i;
    }
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }

    map<int, int> scanners;
    int depth, range;
    char c;
    while(file >> depth)
    {
        file >> c;
        file >> range;
        scanners[depth] = range;
    }
    file.close();

    int severity = 0;
    for(int i=0, seconds=0; i<=depth; i++, seconds++)
    {
        if(!scanners.count(i))
            continue;
        if(scanner_position(scanners.at(i), seconds) == 0)
            severity += i * scanners.at(i);
    }
    cout << "part one: " << severity << endl;
    cout << "part two: " << part2(scanners) << endl;
    return 0;
}
