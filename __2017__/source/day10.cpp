#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <numeric>
#include <algorithm>
#include<iomanip>
using namespace std;

#define len 256

vector<int> reversed(vector<int> input, int repeat)
{
    vector<int> arr(len);
    iota(arr.begin(), arr.end(), 0);
    int pos = 0, shift = 0;

    for(int k=0; k<repeat; k++)
    {
        for(int n: input)
        {
            vector<int> tmp;
            for(int i =pos; i <pos+n; i++)
                tmp.push_back(arr[i % len]);
            for(int i =pos; i <pos+n; i++){
                int x = tmp.back();
                tmp.pop_back();
                arr[i%len] = x;
            }

            pos = (pos + n + shift) % len;
            shift++;
        }
    }
    return arr;
}

vector<int> dense(vector<int> knot)
{
    vector<int> dns(16, 0);
    for(int i=0; i < 16; i++)
    {
        dns[i] = knot[16*i];
        for(int j=1; j<16; j++)
            dns[i] ^= knot[16*i+j];
    }
    return dns;
}

string knot_hash(vector<int> dns)
{
    stringstream kh_hash;
    for(int x: dns)
        kh_hash << setfill ('0') << setw(2) << hex << x;
    return kh_hash.str();
}

int main(int argc, char **argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string data;
    getline(file, data);
    file.close();

    istringstream is(data);
    int n; char c;
    vector<int> input;
    while(is >> n){
        input.push_back(n);
        is >> c;
    }
    vector<int> arr = reversed(input, 1);
    cout << "part one: " << arr[0] * arr[1] << endl;

    vector<int> arr2;
    vector<int> input2 = {17, 31, 73, 47, 23};
    for(char c: data)
        arr2.push_back(c);
    arr2.insert(arr2.end(), input2.begin(), input2.end());

    cout << "part two: " << knot_hash(dense(reversed(arr2,64))) << endl;
    return 0;
}
