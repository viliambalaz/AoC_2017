import sys
import string
from collections import defaultdict

def part_1(commands):
    abc = string.ascii_lowercase[:16]
    registers = dict()
    snd = int()
    for c in abc:
        registers[c] = 0

    i = 0
    while(1):
        cmd = commands[i]
    
        if cmd[0] == "snd":
            try:snd = registers[cmd[1]]
            except KeyError: snd = int(cmd[1])
        elif cmd[0] == "set":
            try:registers[cmd[1]] = registers[cmd[2]]
            except KeyError: registers[cmd[1]] = int(cmd[2])
        elif cmd[0] == "add":
            try:registers[cmd[1]] += registers[cmd[2]]
            except KeyError: registers[cmd[1]] += int(cmd[2])
        elif cmd[0] == "mul":
            try:registers[cmd[1]] *= registers[cmd[2]]
            except KeyError: registers[cmd[1]] *= int(cmd[2])
        elif cmd[0] == "mod":
            try:registers[cmd[1]] %= registers[cmd[2]]
            except KeyError: registers[cmd[1]] %= int(cmd[2])
        elif cmd[0] == "rcv":
            try: X = registers[cmd[1]]
            except KeyError: X = int(cmd[1])
            if X:
                return str(snd)
        elif cmd[0] == "jgz":
            try: X = registers[cmd[1]]
            except KeyError: X = int(cmd[1])
            if X > 0: i += int(cmd[2]) -1
        i += 1
        if i >= len(commands):
            raise ValueError("Any sound received")


class Program:
    def __init__(self, pid, other, instr):
        self.regs = defaultdict(int)
        self.regs['p'] = pid
        self.other = other
        self.instr = instr

        self.ip = 0
        self.buffer = []
        self.terminated = False
        self.blocked = False
        self.sent = 0

        for x in "abcdefghijklmnop":
            self.regs[x] = pid
            #print(x + " : " + str(self.regs[x]))

    def next(self):
        if self.terminated or self.ip < 0 or self.ip >= len(self.instr):
            self.terminated = True
            return
        ins = self.instr[self.ip]
        if ins[0] == 'snd':
            self.other.buffer.append(self.get(ins[1]))
            self.other.blocked = False
            self.sent += 1
        elif ins[0] == 'set':
            self.regs[ins[1]] = self.get(ins[2])
        elif ins[0] == 'add':
            self.regs[ins[1]] += self.get(ins[2])
        elif ins[0] == 'mul':
            self.regs[ins[1]] *= self.get(ins[2])
        elif ins[0] == 'mod':
            self.regs[ins[1]] %= self.get(ins[2])
        elif ins[0] == 'rcv':
            if len(self.buffer) > 0:
                self.regs[ins[1]] = self.buffer.pop(0)
            else:
                self.blocked = True
                return
        elif ins[0] == 'jgz':
            if self.get(ins[1]) > 0:
                self.ip += self.get(ins[2])
                return
        self.ip += 1

    def get(self, v):
        try:
            return int(v)
        except ValueError:
            return self.regs[v]

def part_2(commands):
    p0 = Program(0, None, commands)
    p1 = Program(1, p0, commands)
    p0.other = p1

    while not ((p0.terminated or p0.blocked) and (p1.terminated or p1.blocked)):
        p0.next()
        p1.next()

    return p1.sent

def main():
    if len(sys.argv) != 2:
       print("Wrong arguments")
       return -1
        
    fp = open(sys.argv[1])
    data = [list(line.strip('\n').split()) for line in fp.readlines()]
    fp.close()
    print("part one:", part_1(data))
    print("part one:", part_2(data))


if __name__ == "__main__":
    main()
