#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <set>
#include <vector>
#include <algorithm>
using namespace std;

int part1(ifstream& file)
{
    file.clear();
    file.seekg(0);
    int no = 0;
    string line;
    while(getline(file, line))
    {
        istringstream is(line);
        set<string> st;
        string word;
        while(is >> word){
            if(st.insert(word).second == false){
                no--;
                break;
            }
        }
        no++;
    }
    return no;
}

int part2(ifstream& file)
{
    file.clear();
    file.seekg(0);
    int no = 0;
    string line;
    while(getline(file, line))  //also count empty line
    {
        istringstream is(line);
        set<string> st;
        string word;
        while(is >> word)
        {
            vector<char> vec(word.begin(), word.end());
            sort(vec.begin(), vec.end());
            string s(vec.begin(), vec.end());
            if(st.insert(s).second == false){
                no--;
                break;
            }
        }
        no++;
    }
    return no;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    cout << "part one: " << part1(file) << endl;
    cout << "part one: " << part2(file) << endl;

    file.close();
    return 0;
}

