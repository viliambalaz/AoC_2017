#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>
#include <set>
#include <map>
using namespace std;

void connected(map<int, set<int> >& table, int source, set<int>& pipe)
{
    for(int x: table.at(source))
    {
        if(pipe.insert(x).second == true)
            connected(table, x, pipe);
    }
    return;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }

    map<int, set<int> > table;
    string line;
    while(getline(file, line))
    {
        smatch m;
        regex e("\\w+");
        stringstream ss;
        int id, program;

        while (regex_search(line,m,e)) {
            ss << *(m.begin()) << endl;
            line = m.suffix().str();
        }
        ss >> id;
        while(ss >> program)
            table[id].insert(program);
    }
    file.close();
    int source = 0;
    set<int> pipe;
    pipe.insert(source);
    connected(table, source, pipe);
    cout << "part one: " << pipe.size() << endl;

    int groups = 0;
    set<int> elements;
    for(int i=0; i<2000; i++)
        elements.insert(i);

    while(elements.size())
    {
        pipe.clear();
        source = *elements.begin();
        elements.erase(elements.begin());
        pipe.insert(source);
        connected(table, source, pipe);
        set<int> tmp;
        set_difference(elements.begin(), elements.end(), pipe.begin(), pipe.end(), inserter(tmp, tmp.end()));
        elements = set<int>(tmp);
        groups ++;
    }
    cout << "part two: " << groups << endl;
    return 0;
}
