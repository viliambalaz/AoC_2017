#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>
#include <algorithm>
#include <sstream>
#include <bitset>
#include<iomanip>
#include <exception>
using namespace std;

#define len 256
#define row 128

vector<int> reversed(vector<int> input, int repeat)
{
    vector<int> arr(len);
    iota(arr.begin(), arr.end(), 0);
    int pos = 0, shift = 0;

    for(int k=0; k<repeat; k++)
    {
        for(int n: input)
        {
            vector<int> tmp;
            for(int i =pos; i <pos+n; i++)
                tmp.push_back(arr[i % len]);
            for(int i =pos; i <pos+n; i++){
                int x = tmp.back();
                tmp.pop_back();
                arr[i%len] = x;
            }
            pos = (pos + n + shift) % len;
            shift++;
        }
    }
    return arr;
}

vector<int> dense(vector<int> knot)
{
    vector<int> dns(16, 0);
    for(int i=0; i < 16; i++)
    {
        dns[i] = knot[16*i];
        for(int j=1; j<16; j++)
            dns[i] ^= knot[16*i+j];
    }
    return dns;
}

string knot_hash(vector<int> dns)
{
    stringstream kh_hash;
    for(int x: dns)
        kh_hash << setfill ('0') << setw(2) << hex << x;
    return kh_hash.str();
}

const char* hex_char_to_bin(char c)
{
    // TODO handle default / error
    switch(toupper(c))
    {
        case '0': return "0000";
        case '1': return "0001";
        case '2': return "0010";
        case '3': return "0011";
        case '4': return "0100";
        case '5': return "0101";
        case '6': return "0110";
        case '7': return "0111";
        case '8': return "1000";
        case '9': return "1001";
        case 'A': return "1010";
        case 'B': return "1011";
        case 'C': return "1100";
        case 'D': return "1101";
        case 'E': return "1110";
        case 'F': return "1111";
    }
    throw -1;
}

string hex_str_to_bin_str(const string& hex)
{
    string bin;
    for(unsigned i = 0; i != hex.length(); ++i)
       bin += hex_char_to_bin(hex[i]);
    return bin;
}

void annul_square(vector<vector<int> >& regions, int i, int j)
{
    vector<pair<int,int> >direction ={{-1,0},{0,1},{1,0},{0,-1}};
    for(pair<int,int> p : direction)
    {
        try{
            if(regions.at(i + p.first).at(j + p.second) == 1){
                regions.at(i + p.first).at(j + p.second) = 0;
                annul_square(regions, i + p.first, j + p.second);
            }
        }
        catch(out_of_range msg){;}
    }
}

int squares(vector<vector<int> >& regions)
{
    int sq = 0;
    for(int i=0; i<row; i++)
    {
        for(int j=0; j<row; j++){
            if(regions[i][j] == 1){
                sq++;
                regions[i][j] = 0;
                annul_square(regions, i, j);
            }
        }
    }
    return sq;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string data;
    getline(file, data);
    file.close();

    vector<int> input2 = {17, 31, 73, 47, 23};
    vector<int> arr;
    vector<vector<int> > regions;
    int on = 0;
    for(char c: data)
        arr.push_back(c);

    for(int i=0; i<row; i++)
    {
        vector<int> tmp(arr);
        tmp.push_back('-');
        string s = to_string(i);
        tmp.insert(tmp.end(), s.begin(), s.end());
        tmp.insert(tmp.end(), input2.begin(), input2.end());

        string kh_hash = knot_hash(dense(reversed(tmp,64)));
        string bin = hex_str_to_bin_str(kh_hash);
        on += count(bin.begin(), bin.end(), '1');

        regions.push_back({});
        for(unsigned int k=0; k<bin.length(); k++)
        {
            regions[i].push_back((int)bin[k] - (int)'0');
        }
    }

    cout << "part one: " << on << endl;
    cout << "part two: " << squares(regions) << endl;
    return 0;
}
