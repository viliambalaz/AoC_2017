#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string data;
    getline(file, data);
    file.close();

    int weight = 0, score = 0, non_canceled = 0;
    bool garbage = false;

    for(unsigned int i=0; i< data.length(); i++)
    {
        if(data[i] == '!'){
            i++; //ignore next character
            continue;
        }
        else if(data[i] == '>')
            garbage = false;

        if(!garbage)
        {
            if(data[i] == '<')
                garbage = true;
            else if(data[i] == '{')
                weight++;
            else if(data[i] == '}'){
                score += weight;
                weight--;
            }
        }
        else
            non_canceled++;
    }

    cout << "part one: " << score << endl;
    cout << "part two: " << non_canceled << endl;
    return 0;
}

