#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
using namespace std;

int level(const int sn)
{
    /// 4n^2 + 4n - (sn - 1) = 0
    return (int)ceil((1+sqrt(sn))/2.0) - 1;
}

int end_point(int n)
{
    /// 4n^2 + 4n
    return 4*n*(1+n) + 1;
}

vector<int> horizontally_distance(const int n)
{
    vector<int> tmp;
    for(int i=n; i >=0; i--)
        tmp.push_back(i);
    for(int i=1; i <= n-1; i++)
        tmp.push_back(i);
    return tmp;
}

int part1(const int sn)
{
    int n = level(sn);
    int ep = end_point(n);
    vector<int> side = horizontally_distance(n);
    return n + side[(ep-sn)%side.size()];
}

int get_next(map<pair<int, int>, int>& grid, int x ,int y)
{
    int result = 0;
    vector<int> i_direct = {-1, 0, 1};
    vector<int> j_direct = {-1, 0, 1};
    for(int i: i_direct)
        for(int j: j_direct)
        {
            if(!i && !j) continue;
            if(grid.count(pair<int, int>(x+i,y+j)))
                result += grid[pair<int, int>(x+i, y+j)];
        }
    return result;
}

int part2(int input)
{
    int number = input;
    map<pair<int, int>, int> grid;
    grid[pair<int, int>(0,0)] = 1;
    int x = 1, y = -1, direction = 0,  levelSide = 2;

    while(1)
    {
        for(int i=0; i<4; i++)
        {
            for(int j=0; j < levelSide; j++){
                switch (direction % 4){
                case 0:
                    y++;
                    break;
                case 1:
                    x--;
                    break;
                case 2:
                    y--;
                    break;
                case 3:
                    x++;
                    break;
                }
                grid[pair<int, int>(x,y)] = get_next(grid, x,y);
                if(grid[pair<int,int>(x,y)] > number)
                        return grid[pair<int,int>(x,y)];
            }
            direction = (direction + 1) % 4;
        }
    ++x;
    --y;
    levelSide += 2;
    }
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    int number;
    file >> number;
    file.close();

    cout << "part one: " << part1(number) << endl;
    cout << "part two: " << part2(number) << endl;
    return 0;
}

