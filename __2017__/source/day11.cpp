#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <vector>
#include <sstream>
using namespace std;

int cube_distance(vector<int> a, vector<int> b)
{
    return (abs(a[0] - b[0]) + abs(a[1] - b[1]) + abs(a[2] - b[2])) / 2;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string data;
    getline(file, data);
    istringstream is(data+",");
    file.close();

    map<string, vector<int> > mp;
    mp["n"] = {1,-1,0};
    mp["nw"] = {1,0,-1};
    mp["ne"] = {0,-1,1};
    mp["s"] = {-1,1,0};
    mp["sw"] = {0,1,-1};
    mp["se"] = {-1,0,1};

    vector<int> current = {0,0,0};
    string direction;
    char c;
    int dst,dst_max,x,y,z;
    dst = dst_max = x = y = z = 0;

    while(is >> c)
    {
        if(c == ','){
            x += mp.at(direction)[0];
            y += mp.at(direction)[1];
            z += mp.at(direction)[2];
            direction = "";
            dst = cube_distance({0,0,0}, {x,y,z});
            if(dst > dst_max) dst_max = dst;
        }
        else
            direction += c;
    }
    cout << "part one: " << dst << endl;
    cout << "part two: " << dst_max << endl;
    return 0;
}
