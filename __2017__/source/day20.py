import re, time, sys

def sum_abs(part):
    return sum([abs(p) for p in part])


def main():
    
    if len(sys.argv) != 2:
       print("Wrong arguments")
       return -1

    pattern = r"(-?\d+,?){3}"
    particles = []
    with open(sys.argv[1]) as fp:
        for i,line in enumerate(fp):
            refier = re.finditer(pattern, line)
            particles.append([list(map(int, x.group().split(','))) for x in refier] + [i])

    particles.sort(key=lambda x: (sum_abs(x[2]), sum_abs(x[1]), sum_abs(x[1])))
    print("part one:", particles[0][3])

    for _ in range(100): 
        # calc new coordinates
        for i in range(len(particles)):
            for j in range(3):
                particles[i][1][j] += particles[i][2][j]
                particles[i][0][j] += particles[i][1][j]

        # remove 2+ occurences
        position = [particles[i][0] for i in range(len(particles))]
        to_remove = []
        for i in range(len(position)):
            if position.count(position[i]) >= 2:
                to_remove.append(i)
        while(to_remove):
            del particles[to_remove.pop()]

    print("part two:", len(particles))

if __name__ == "__main__":
    main()
