#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int calc_sum(string data, int step)
{
    int sum = 0;
    int len = data.length();
    for(int i=0; i < len; i++){
        if(data[i] == data[(i+step)%len])
            sum += data[i] - '0';
    }
    return sum;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string data;
    getline(file, data);
    file.close();

    cout << "part 1: " << calc_sum(data, 1) << endl;
    cout << "part 2: " << calc_sum(data, data.length()/2) << endl;
    return 0;
}
