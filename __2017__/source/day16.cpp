#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
using namespace std;

#define LOOP 1000000000

string part1(ifstream& file, string& dance)
{
    file.clear();
    file.seekg(0);
    string command;
    while(getline(file, command, ','))
    {
        istringstream ss(command);
        char c, a, b;
        int x,y;

        switch(command[0])
        {

            case 's':{
                ss >> c >> x;
                dance = dance.substr(dance.length() - x) + dance.substr(0, dance.length() -x);
                break;
            }
            case 'x':{
                ss >> c >> x >> c >> y;
                swap(dance[x], dance[y]);
                break;
            }
            case 'p':{
                ss >> c >> a >> c >> b;
                swap(dance[dance.find(a)], dance[dance.find(b)]);
                break;
            }
        }
    }
    return dance;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    string start_pos = "abcdefghijklmnop";
    string dance(start_pos);

    cout << "part one: " << part1(file, dance) << endl;

    start_pos = string(dance);
    for(long i=0; i<LOOP; i++){
            //cout << dance << endl;
        part1(file, dance);

        if(dance == start_pos){
            for(int j=0; j<LOOP%(i+1)-1; j++){
                part1(file, dance);
            }
            break;
        }
    }
    cout << "part two: " << dance << endl;
    file.close();
    return 0;
}
