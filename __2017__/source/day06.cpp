#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
using namespace std;

int max_blocks(vector<int>& registers)
{
    int m = -1, j = 0;
    for(unsigned int i=0; i<registers.size(); i++){
        if(registers[i] > m){
            m = registers[i];
            j = i;
        }
    }
    return j;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    vector<int> registers;
    int n;
    while(file >> n)
        registers.push_back(n);
    file.close();
    int len = registers.size();
    set<vector<int> > cycles;
    int steps = 0;
    map<vector<int> , int> again;

    while(1)
    {
        steps++;
        int j = max_blocks(registers);
        int blocks = registers[j];
        registers[j] = 0;
        for(int i=1; i<=blocks; i++){
            registers[(i+j)%len]++;
        }
        if(cycles.insert(registers).second == false){
            cout << "part one: " << steps << endl;
            cout << "part two: " << steps - again[registers] << endl;
            return 0;
        }
        again.insert(pair<vector<int>, int>(registers,steps));
    }
    return -3;
}

