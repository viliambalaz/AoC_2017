#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

int part1(ifstream& file)
{
    file.clear();
    file.seekg(0);
    vector<int> vec;
    int n;
    while(file >> n)
        vec.push_back(n);
    int steps = 0;
    int i = 0;

    while((i < (int)vec.size()) || (i < 0))
    {
        vec.at(i)++;
        i+= vec.at(i) - 1;
        steps++;
    }
    return steps;
}

int part2(ifstream& file)
{
    file.clear();
    file.seekg(0);
    vector<int> vec;
    int n;
    while(file >> n)
        vec.push_back(n);
    int steps = 0;
    int i = 0;

    while((i < (int)vec.size()) || (i < 0))
    {
        if(vec.at(i) >= 3){
            vec.at(i)--;
            i+= vec.at(i) + 1;
            steps++;
        }
        else{
            vec.at(i)++;
            i+= vec.at(i) - 1;
            steps++;
        }
    }
    return steps;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    cout << "part one: " << part1(file) << endl;
    cout << "part two: " << part2(file) << endl;

    file.close();
    return 0;
}

