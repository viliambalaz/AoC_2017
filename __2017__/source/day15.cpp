#include <iostream>
#include <fstream>
#include <string>
#include <regex>
#include <sstream>
#include <bitset>
using namespace std;

#define FACTOR_A 16807
#define FACTOR_B 48271
#define MOD 2147483647
#define LOOP 40000000
#define LOOP2 5000000
#define MOD_A 4
#define MOD_B 8

int part1(long A, long B)
{
    int pairs = 0;
    for(int i=0; i<LOOP; i++)
    {
        A *= FACTOR_A;
        A %= MOD;

        B *= FACTOR_B;
        B %= MOD;

        bitset<16> bit_A(A);
        bitset<16> bit_B(B);

        if(bit_A == bit_B)
            pairs++;
    }
    return pairs;
}

int part2(long A, long B)
{
    int pairs = 0;
    for(int i=0; i<LOOP2; i++)
    {
        do{
        A *= FACTOR_A;
        A %= MOD;
        } while(A % MOD_A);

        do{
        B *= FACTOR_B;
        B %= MOD;
        } while(B % MOD_B);

        bitset<16> bit_A(A);
        bitset<16> bit_B(B);

        if(bit_A == bit_B)
            pairs++;
    }
    return pairs;
}

int main(int argc, char** argv)
{
    if(argc != 2){
        cout << "Wrong arguments" << endl;
        return -1;
    }
    ifstream file(argv[1]);
    if(!file.is_open()){
        cout << "No such file." << endl;
        return -2;
    }
    stringstream ss;
    string s;
    regex e ("\\d+");
    smatch m;

    string line;
    while(getline(file, line))
    {
        regex_search(line,m,e);
        ss << *m.begin() << " ";
    }
    file.close();
    long A, B;
    ss >> A >> B;

    cout << "part one: " << part1(A, B) << endl;
    cout << "part two: " << part2(A, B) << endl;
    return 0;
}
