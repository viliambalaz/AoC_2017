# Year 2017
Days 1 - 17 written in C++, days 18 - 25 in python3.
- `g++ -std=gnu++11 day^^.cpp`
- `./a.out input_file`
- `python3 path_to_source/day^^.py input_file` 

## Execution time
- day01 - 0.00s
- day02 - 0.00s
- day03 - 0.00s
- day04 - 0.01s
- day05 - 1.53s
- day06 - 0.04s
- day07 - 0.04s
- day08 - 0.00s
- day09 - 0.00s
- day10 - 0.02s
- day11 - 0.01s
- day12 - 0.64s
- day13 - 0.52s
- day14 - 1.09s
- day15 - 2.42s
- day16 - 0.93s
- day17 - 0.63s
- day18 - 0.68s
- day19 - 0.07s
- day20 - 2.82s
- day21 - 15.74s
- day22 - 18.95s
- day23 - 1.64s
- day24 - 12.54s
- day25 - 7.67s

