# Advent of Code
[Advent of Code](https://adventofcode.com) is a series of small programming puzzles for a variety of skill levels. They are self-contained and are just as appropriate for an expert who wants to stay sharp as they are for a beginner who is just learning to code. Each puzzle calls upon different skills and has two parts that build on a theme.

## Solutions
 These are not official solutions.
Curently solved problems.
- 2015: all soultions in C++ 
- 2016: not available yet.
- 2017: Days 1 - 17 in C++, 18 - 25 in python3.

## Run
Set program parameter to the relative path of the filename with puzzle input. Make sure that input does not contain blank line at the end of the file.

